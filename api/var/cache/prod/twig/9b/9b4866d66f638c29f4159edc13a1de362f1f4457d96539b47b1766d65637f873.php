<?php

/* EDUBundle:users:layout_all_users.html.twig */
class __TwigTemplate_2828fe8141e0df43275f0bfc4e2f7435ee48946da0b0884675241cd21e074e9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EDUBundle::layout.html.twig", "EDUBundle:users:layout_all_users.html.twig", 1);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EDUBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_cmi_body($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["base_url"] ?? null), "html", null, true);
        echo "/user/add\" class=\"btn btn-success btn-xs\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Add New User"), "html", null, true);
        echo "</a>
            <br /><br />
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Name"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Email"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Department"), "html", null, true);
        // line 16
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Actions"), "html", null, true);
        echo "</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Name"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Email"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Department"), "html", null, true);
        // line 25
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Actions"), "html", null, true);
        echo "</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["users"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 31
            echo "                            ";
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "enabled", array())) {
                // line 32
                echo "                                ";
                if (((null === twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "updated", array())) ||  !twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "updated", array()))) {
                    // line 33
                    echo "                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            ";
                    // line 35
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastname", array()), "html", null, true);
                    echo "
                                        </td>
                                        <td>
                                            ";
                    // line 38
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
                    echo "
                                        </td>
                                        <td>
                                           ";
                    // line 41
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "department", array()), "html", null, true);
                    echo "  
                                        </td>
                                        <td>
                                            
                                            <input type=\"buton\" class=\"btn btn-xs btn-primary\" style=\"width:69px;\" value=\"";
                    // line 45
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("View"), "html", null, true);
                    echo "\"  onclick=\"detail(";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo ")\">
                                            </input>  
                                            
                                            
                                            <a href=\"";
                    // line 49
                    echo twig_escape_filter($this->env, ($context["base_url"] ?? null), "html", null, true);
                    echo "/user/edit/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-default\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
                    echo "</a>
                                            <a href=\"";
                    // line 50
                    echo twig_escape_filter($this->env, ($context["base_url"] ?? null), "html", null, true);
                    echo "/user/delete/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-danger\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Delete"), "html", null, true);
                    echo "</a>
                                        </td>
                                    </tr>
                                ";
                } else {
                    // line 54
                    echo "                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            <b>";
                    // line 56
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastname", array()), "html", null, true);
                    echo "</b>
                                        </td>
                                        <td>
                                            <b>";
                    // line 59
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
                    echo "</b>
                                        </td>
                                        <td>
                                            <b>";
                    // line 62
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "phoneNumber", array()), "html", null, true);
                    echo "</b>
                                        </td>
                                        <td>
                                           
                                              <input type=\"buton\" class=\"btn btn-xs btn-primary\" style=\"width:69px;\" value=\"";
                    // line 66
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("View"), "html", null, true);
                    echo "\"  onclick=\"detail(";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo ")\">
                                             </input>  
                                            
                                            
                                            <a href=\"";
                    // line 70
                    echo twig_escape_filter($this->env, ($context["base_url"] ?? null), "html", null, true);
                    echo "/user/edit/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-default\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
                    echo "</a>
                                            <a href=\"";
                    // line 71
                    echo twig_escape_filter($this->env, ($context["base_url"] ?? null), "html", null, true);
                    echo "/user/delete/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-danger\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Delete"), "html", null, true);
                    echo "</a>
                                        </td>
                                    </tr>
                                ";
                }
                // line 75
                echo "                            ";
            }
            // line 76
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!--moodle-->


  ";
        // line 86
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["users"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 87
            echo "    ";
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "enabled", array())) {
                // line 88
                echo "    ";
                if (((null === twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "updated", array())) ||  !twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "updated", array()))) {
                    echo "   
    
    
    <div class=\"container\">
  <!-- Modal -->
  <div class=\"modal fade\" id=\"detail_";
                    // line 93
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" role=\"dialog\" tabindex=\"-1\">
    
  
    <div class=\"modal-dialog\" role=\"document\">
      <!-- Modal content-->
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;
          </button>
          <h4 class=\"modal-title\"><b>";
                    // line 102
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user-all-popup-heading"), "html", null, true);
                    echo "</b>
          </h4>
        </div>
        <div class=\"modal-body\">
          <!--<a href=\"";
                    // line 106
                    echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_teachers");
                    echo "\" class=\"btn btn-sm btn-primary\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Go Back"), "html", null, true);
                    echo "
          </a>-->
           <a href=\"";
                    // line 108
                    echo twig_escape_filter($this->env, ($context["base_url"] ?? null), "html", null, true);
                    echo "/user/edit/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-sm btn-default\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
                    echo " </a>
          </a>
          <br/>
          <br/>
          <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
             <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <tbody>
                            <tr class=\"gradeA odd\" role=\"row\">
                                <td>
                                    ";
                    // line 117
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Username"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 120
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
                    echo " 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 125
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Name"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 128
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastname", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 133
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Email"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 136
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <!-- new-->
                            <tr>
                                <td>
                                    ";
                    // line 142
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Role"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                
                                ";
                    // line 146
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "getRoles", array(), "method"));
                    foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                        // line 147
                        echo "                                      
                   
                                        ";
                        // line 149
                        if (($context["user"] == "ROLE_ADMIN")) {
                            // line 150
                            echo "                                               <span> ";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_admin"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 152
                        echo "                                        ";
                        if (($context["user"] == "EMPLOYEE")) {
                            // line 153
                            echo "                                               <span> ";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("EMPLOYEE"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 155
                        echo "                                     
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 157
                    echo "                                      
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    ";
                    // line 163
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Department"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                     ";
                    // line 166
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "department", array()), "html", null, true);
                    echo " 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 171
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Language"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 174
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "fr")) {
                        // line 175
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_france"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 177
                    echo "                                    ";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "nl")) {
                        // line 178
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_netherland"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 180
                    echo "                                     ";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "en")) {
                        // line 181
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_english"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 183
                    echo "                                    
                                    
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    ";
                    // line 189
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Address"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 192
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "address", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    ";
                    // line 197
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Postcode"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 200
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "postCode", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                              
                                <tr>
                                <td>
                                    ";
                    // line 206
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-conatct_number"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 209
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "contactNumber", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                        
                            <!--  -->
                            <tr>
                                <td>
                                    ";
                    // line 216
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-phoneNumber"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 219
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "phoneNumber", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 224
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-lastLogin"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 227
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastlogin", array()), "Y-m-d H:i:s"), "html", null, true);
                    echo "
                                </td>
                            </tr>
                        </tbody>
                    </table>
        </div>
        <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
    ";
                } else {
                    // line 242
                    echo "         <div class=\"container\">
  <!-- Modal -->
  <div class=\"modal fade\" id=\"detail_";
                    // line 244
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" role=\"dialog\" tabindex=\"-1\">
    
  
    <div class=\"modal-dialog\" role=\"document\">
      <!-- Modal content-->
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;
          </button>
          <h4 class=\"modal-title\"><b>";
                    // line 253
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user-all-popup-heading"), "html", null, true);
                    echo "</b>
          </h4>
        </div>
        <div class=\"modal-body\">
          <!--<a href=\"";
                    // line 257
                    echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_teachers");
                    echo "\" class=\"btn btn-sm btn-primary\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Go Back"), "html", null, true);
                    echo "
          </a>-->
           <a href=\"";
                    // line 259
                    echo twig_escape_filter($this->env, ($context["base_url"] ?? null), "html", null, true);
                    echo "/user/edit/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-sm btn-default\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
                    echo " </a>
          </a>
          <br/>
          <br/>
          <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
             <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <tbody>
                            <tr class=\"gradeA odd\" role=\"row\">
                                <td>
                                    ";
                    // line 268
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Username"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 271
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
                    echo " 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 276
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Name"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 279
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastname", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 284
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Email"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 287
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    ";
                    // line 292
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Role"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                   ";
                    // line 295
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "getRoles", array(), "method"));
                    foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                        // line 296
                        echo "                                      
                                        ";
                        // line 297
                        if (($context["user"] == "ROLE_ADMIN")) {
                            // line 298
                            echo "                                               <span> ";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_admin"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 300
                        echo "                                        ";
                        if (($context["user"] == "ROLE_PARENT")) {
                            // line 301
                            echo "                                               <span> ";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_parent"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 303
                        echo "                                        ";
                        if (($context["user"] == "ROLE_TEACHER")) {
                            echo "          
                                               <span> ";
                            // line 304
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_teacher"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 306
                        echo "                                        ";
                        if (($context["user"] == "ROLE_STUDENT")) {
                            echo "          
                                               <span> ";
                            // line 307
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_student"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 309
                        echo "                                   
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 311
                    echo "                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 315
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Language"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 318
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "fr")) {
                        // line 319
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_france"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 321
                    echo "                                    ";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "nl")) {
                        // line 322
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_netherland"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 324
                    echo "                                    ";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "en")) {
                        // line 325
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_english"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 327
                    echo "                                    
                                    
                            
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    ";
                    // line 334
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Address"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 337
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "address", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    ";
                    // line 342
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Postcode"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 345
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "postCode", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                              
                                <tr>
                                <td>
                                    ";
                    // line 351
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-conatct_number"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 354
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "contactNumber", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                                
                            <tr>
                                <td>
                                    ";
                    // line 360
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.phoneNumber"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 363
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "phoneNumber", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 368
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-lastLogin"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 371
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastlogin", array()), "Y-m-d H:i:s"), "html", null, true);
                    echo "
                                </td>
                            </tr>
                        </tbody>
                    </table>
        </div>
        <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
   
";
                }
                // line 387
                echo " 
";
            }
            // line 389
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 390
        echo "  
    
";
    }

    public function getTemplateName()
    {
        return "EDUBundle:users:layout_all_users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  803 => 390,  796 => 389,  792 => 387,  773 => 371,  767 => 368,  759 => 363,  753 => 360,  744 => 354,  738 => 351,  729 => 345,  723 => 342,  715 => 337,  709 => 334,  700 => 327,  694 => 325,  691 => 324,  685 => 322,  682 => 321,  676 => 319,  674 => 318,  668 => 315,  662 => 311,  655 => 309,  650 => 307,  645 => 306,  640 => 304,  635 => 303,  629 => 301,  626 => 300,  620 => 298,  618 => 297,  615 => 296,  611 => 295,  605 => 292,  597 => 287,  591 => 284,  581 => 279,  575 => 276,  567 => 271,  561 => 268,  543 => 259,  536 => 257,  529 => 253,  517 => 244,  513 => 242,  495 => 227,  489 => 224,  481 => 219,  475 => 216,  465 => 209,  459 => 206,  450 => 200,  444 => 197,  436 => 192,  430 => 189,  422 => 183,  416 => 181,  413 => 180,  407 => 178,  404 => 177,  398 => 175,  396 => 174,  390 => 171,  382 => 166,  376 => 163,  368 => 157,  361 => 155,  355 => 153,  352 => 152,  346 => 150,  344 => 149,  340 => 147,  336 => 146,  329 => 142,  320 => 136,  314 => 133,  304 => 128,  298 => 125,  290 => 120,  284 => 117,  266 => 108,  259 => 106,  252 => 102,  240 => 93,  231 => 88,  228 => 87,  224 => 86,  213 => 77,  207 => 76,  204 => 75,  193 => 71,  185 => 70,  176 => 66,  169 => 62,  163 => 59,  155 => 56,  151 => 54,  140 => 50,  132 => 49,  123 => 45,  116 => 41,  110 => 38,  102 => 35,  98 => 33,  95 => 32,  92 => 31,  88 => 30,  81 => 26,  78 => 25,  76 => 24,  72 => 23,  68 => 22,  60 => 17,  57 => 16,  55 => 15,  51 => 14,  47 => 13,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "EDUBundle:users:layout_all_users.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/users/layout_all_users.html.twig");
    }
}
