<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_239c56cf8fc3327bbc3674a749b4ba943b708902a12603c446efcd3b1873669c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_87449e0480cf3c705ab2840dbed19fcf116aeaf5360e62d1fcd09c6fbf464f5a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_87449e0480cf3c705ab2840dbed19fcf116aeaf5360e62d1fcd09c6fbf464f5a->enter($__internal_87449e0480cf3c705ab2840dbed19fcf116aeaf5360e62d1fcd09c6fbf464f5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_87449e0480cf3c705ab2840dbed19fcf116aeaf5360e62d1fcd09c6fbf464f5a->leave($__internal_87449e0480cf3c705ab2840dbed19fcf116aeaf5360e62d1fcd09c6fbf464f5a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d2370aa7fa0ffe6ad91301a9c074b41ce93a9355222e5741807eb1098891c2fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d2370aa7fa0ffe6ad91301a9c074b41ce93a9355222e5741807eb1098891c2fc->enter($__internal_d2370aa7fa0ffe6ad91301a9c074b41ce93a9355222e5741807eb1098891c2fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_d2370aa7fa0ffe6ad91301a9c074b41ce93a9355222e5741807eb1098891c2fc->leave($__internal_d2370aa7fa0ffe6ad91301a9c074b41ce93a9355222e5741807eb1098891c2fc_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "/var/www/parkway/api/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request.html.twig");
    }
}
