<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_ccdaf2ac67bb7368302387e7b84ef1a14af19a5ab8a1907e4f734b1784c539ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_676d358d4c3cdd92e4e106db225449cf29dc209d9e459654df463b098777e556 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_676d358d4c3cdd92e4e106db225449cf29dc209d9e459654df463b098777e556->enter($__internal_676d358d4c3cdd92e4e106db225449cf29dc209d9e459654df463b098777e556_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_676d358d4c3cdd92e4e106db225449cf29dc209d9e459654df463b098777e556->leave($__internal_676d358d4c3cdd92e4e106db225449cf29dc209d9e459654df463b098777e556_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
