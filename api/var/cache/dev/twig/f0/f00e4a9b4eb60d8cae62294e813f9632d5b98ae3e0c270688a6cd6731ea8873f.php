<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_fc61bd56190b32d7066b4527078e1bb46b9a0edef34fe7453448b3aabb755e43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_35ab715b9fe16f522399214cb13d80dd991211885f254f5368c0cf87fd677b14 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_35ab715b9fe16f522399214cb13d80dd991211885f254f5368c0cf87fd677b14->enter($__internal_35ab715b9fe16f522399214cb13d80dd991211885f254f5368c0cf87fd677b14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_35ab715b9fe16f522399214cb13d80dd991211885f254f5368c0cf87fd677b14->leave($__internal_35ab715b9fe16f522399214cb13d80dd991211885f254f5368c0cf87fd677b14_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
