<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_ac36c0f832f575947775b5a9d2c54aadee9cd7e1d5527916185769ce2a9d5bf0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0dd03006152961f77ac1f562be02026d4d80f9257439f0edf6098e71107c863 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a0dd03006152961f77ac1f562be02026d4d80f9257439f0edf6098e71107c863->enter($__internal_a0dd03006152961f77ac1f562be02026d4d80f9257439f0edf6098e71107c863_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_a0dd03006152961f77ac1f562be02026d4d80f9257439f0edf6098e71107c863->leave($__internal_a0dd03006152961f77ac1f562be02026d4d80f9257439f0edf6098e71107c863_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
