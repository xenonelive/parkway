<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_5f4fdf30c5cb3624a40c62d5b8e8666c31bfabb1f478d999d69411d6af7d02cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3bd05ff8a5ba8802ad3171def64416e40e81a5ee441fd6fa3fa2ab714a331f89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3bd05ff8a5ba8802ad3171def64416e40e81a5ee441fd6fa3fa2ab714a331f89->enter($__internal_3bd05ff8a5ba8802ad3171def64416e40e81a5ee441fd6fa3fa2ab714a331f89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_3bd05ff8a5ba8802ad3171def64416e40e81a5ee441fd6fa3fa2ab714a331f89->leave($__internal_3bd05ff8a5ba8802ad3171def64416e40e81a5ee441fd6fa3fa2ab714a331f89_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
