<?php

/* EDUBundle:departments:layout_all_audit.html.twig */
class __TwigTemplate_023b06b1b1c5c5e3f4826271ed9ab4d2f804d282fd3cb3ea0ea9853effe0dced extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EDUBundle::layout.html.twig", "EDUBundle:departments:layout_all_audit.html.twig", 1);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EDUBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46aa5eab82d9bf45ae7022e176cc7dc345e145e85646af6d9179b404e2d866e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46aa5eab82d9bf45ae7022e176cc7dc345e145e85646af6d9179b404e2d866e4->enter($__internal_46aa5eab82d9bf45ae7022e176cc7dc345e145e85646af6d9179b404e2d866e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:departments:layout_all_audit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_46aa5eab82d9bf45ae7022e176cc7dc345e145e85646af6d9179b404e2d866e4->leave($__internal_46aa5eab82d9bf45ae7022e176cc7dc345e145e85646af6d9179b404e2d866e4_prof);

    }

    // line 3
    public function block_cmi_body($context, array $blocks = array())
    {
        $__internal_ab60c7df3b337b87beee770f23228569b561f0fedbc60bb59e44161d47233682 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab60c7df3b337b87beee770f23228569b561f0fedbc60bb59e44161d47233682->enter($__internal_ab60c7df3b337b87beee770f23228569b561f0fedbc60bb59e44161d47233682_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "cmi_body"));

        // line 4
        echo "    <div class=\"row\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Action"), "html", null, true);
        // line 11
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Target User"), "html", null, true);
        // line 13
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Date"), "html", null, true);
        // line 15
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("by"), "html", null, true);
        // line 17
        echo "</th>
                            </tr>
                        </thead>
                        <tbody>
                        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["logs"]) || array_key_exists("logs", $context) ? $context["logs"] : (function () { throw new Twig_Error_Runtime('Variable "logs" does not exist.', 21, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["log"]) {
            // line 22
            echo "                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            ";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["log"], "action", array()), "html", null, true);
            echo "
                                        </td>
                                        <td>
                                            ";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["log"], "getTargetuser", array()), "username", array()), "html", null, true);
            echo "
                                        </td>
                                        <td>
                                            ";
            // line 30
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["log"], "getDatetime", array()), "Y-m-d H:i:s"), "html", null, true);
            echo "
                                        </td>
                                        <td>
                                            ";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["log"], "getUser", array()), "username", array()), "html", null, true);
            echo "
                                        </td>
                                    </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['log'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
";
        
        $__internal_ab60c7df3b337b87beee770f23228569b561f0fedbc60bb59e44161d47233682->leave($__internal_ab60c7df3b337b87beee770f23228569b561f0fedbc60bb59e44161d47233682_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:departments:layout_all_audit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 37,  97 => 33,  91 => 30,  85 => 27,  79 => 24,  75 => 22,  71 => 21,  65 => 17,  63 => 16,  60 => 15,  58 => 14,  55 => 13,  53 => 12,  50 => 11,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EDUBundle::layout.html.twig\" %}

{% block cmi_body %}
    <div class=\"row\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{
'Action'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{
'Target User'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{
'Date'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{
'by'|trans }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        {% for log in logs %}
                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            {{ log.action }}
                                        </td>
                                        <td>
                                            {{ log.getTargetuser.username }}
                                        </td>
                                        <td>
                                            {{ log.getDatetime|date('Y-m-d H:i:s')  }}
                                        </td>
                                        <td>
                                            {{ log.getUser.username  }}
                                        </td>
                                    </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
{% endblock %}
", "EDUBundle:departments:layout_all_audit.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/departments/layout_all_audit.html.twig");
    }
}
