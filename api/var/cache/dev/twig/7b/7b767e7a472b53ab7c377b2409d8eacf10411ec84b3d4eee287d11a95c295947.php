<?php

/* :departments:new.html.twig */
class __TwigTemplate_74865afa0deaba67c6fe329e059161fa4ce0d0311dd87b4b9802ef7416070f1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":departments:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_729ba52f36a7db146c7b4ef1f3214b21e1f3b6707c01ecbe06c4f890118fa993 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_729ba52f36a7db146c7b4ef1f3214b21e1f3b6707c01ecbe06c4f890118fa993->enter($__internal_729ba52f36a7db146c7b4ef1f3214b21e1f3b6707c01ecbe06c4f890118fa993_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":departments:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_729ba52f36a7db146c7b4ef1f3214b21e1f3b6707c01ecbe06c4f890118fa993->leave($__internal_729ba52f36a7db146c7b4ef1f3214b21e1f3b6707c01ecbe06c4f890118fa993_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_81913fa957ee8895cafefaf2f043de38977c018c82197811fcd437ded46e25da = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_81913fa957ee8895cafefaf2f043de38977c018c82197811fcd437ded46e25da->enter($__internal_81913fa957ee8895cafefaf2f043de38977c018c82197811fcd437ded46e25da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Department creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 6, $this->getSourceContext()); })()), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 7, $this->getSourceContext()); })()), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext()); })()), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("departments_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_81913fa957ee8895cafefaf2f043de38977c018c82197811fcd437ded46e25da->leave($__internal_81913fa957ee8895cafefaf2f043de38977c018c82197811fcd437ded46e25da_prof);

    }

    public function getTemplateName()
    {
        return ":departments:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 13,  53 => 9,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Department creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('departments_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", ":departments:new.html.twig", "/var/www/parkway/api/app/Resources/views/departments/new.html.twig");
    }
}
