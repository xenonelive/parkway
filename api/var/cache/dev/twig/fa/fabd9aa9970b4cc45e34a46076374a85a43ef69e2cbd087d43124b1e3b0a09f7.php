<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_da87a9478c53cef4428498c378e4af01474372813f645318957ee620fc8e7f2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1fecb0220172ea73f826d5205a3c72d8c4fba7f2eb0445849df1fa6727c12dfb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1fecb0220172ea73f826d5205a3c72d8c4fba7f2eb0445849df1fa6727c12dfb->enter($__internal_1fecb0220172ea73f826d5205a3c72d8c4fba7f2eb0445849df1fa6727c12dfb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_1fecb0220172ea73f826d5205a3c72d8c4fba7f2eb0445849df1fa6727c12dfb->leave($__internal_1fecb0220172ea73f826d5205a3c72d8c4fba7f2eb0445849df1fa6727c12dfb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}
