<?php

/* NelmioApiDocBundle::resource.html.twig */
class __TwigTemplate_9a9a7649ac992665f4b012722149e0f51e60dbaadedaa9cf055cb7e45e3f4b0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("NelmioApiDocBundle::layout.html.twig", "NelmioApiDocBundle::resource.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NelmioApiDocBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66b6441e569497a3b195c8e243519897cb30be59aef89d6a8e5523fadfdf7d91 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_66b6441e569497a3b195c8e243519897cb30be59aef89d6a8e5523fadfdf7d91->enter($__internal_66b6441e569497a3b195c8e243519897cb30be59aef89d6a8e5523fadfdf7d91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "NelmioApiDocBundle::resource.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_66b6441e569497a3b195c8e243519897cb30be59aef89d6a8e5523fadfdf7d91->leave($__internal_66b6441e569497a3b195c8e243519897cb30be59aef89d6a8e5523fadfdf7d91_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_53236ae0ac7ef1269d3d8c673b79c5091e53bf8d9ff9cfcf242e743f3f29c746 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_53236ae0ac7ef1269d3d8c673b79c5091e53bf8d9ff9cfcf242e743f3f29c746->enter($__internal_53236ae0ac7ef1269d3d8c673b79c5091e53bf8d9ff9cfcf242e743f3f29c746_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <li class=\"resource\">
        <ul class=\"endpoints\">
            <li class=\"endpoint\">
                <ul class=\"operations\">
                    ";
        // line 8
        $this->loadTemplate("NelmioApiDocBundle::method.html.twig", "NelmioApiDocBundle::resource.html.twig", 8)->display($context);
        // line 9
        echo "                </ul>
            </li>
        </ul>
    </li>
";
        
        $__internal_53236ae0ac7ef1269d3d8c673b79c5091e53bf8d9ff9cfcf242e743f3f29c746->leave($__internal_53236ae0ac7ef1269d3d8c673b79c5091e53bf8d9ff9cfcf242e743f3f29c746_prof);

    }

    public function getTemplateName()
    {
        return "NelmioApiDocBundle::resource.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 9,  46 => 8,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"NelmioApiDocBundle::layout.html.twig\" %}

{% block content %}
    <li class=\"resource\">
        <ul class=\"endpoints\">
            <li class=\"endpoint\">
                <ul class=\"operations\">
                    {% include 'NelmioApiDocBundle::method.html.twig' %}
                </ul>
            </li>
        </ul>
    </li>
{% endblock content %}
", "NelmioApiDocBundle::resource.html.twig", "/var/www/parkway/api/vendor/nelmio/api-doc-bundle/Nelmio/ApiDocBundle/Resources/views/resource.html.twig");
    }
}
