<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_77e2794db49fee0b07ca557a2f674ea381c4186208316a0b6f28bf54a7933e72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_86992e9f8271066b1c985aeb89d5c2aaa999ef98bacc7b13cfddff2b7cf41317 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86992e9f8271066b1c985aeb89d5c2aaa999ef98bacc7b13cfddff2b7cf41317->enter($__internal_86992e9f8271066b1c985aeb89d5c2aaa999ef98bacc7b13cfddff2b7cf41317_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_86992e9f8271066b1c985aeb89d5c2aaa999ef98bacc7b13cfddff2b7cf41317->leave($__internal_86992e9f8271066b1c985aeb89d5c2aaa999ef98bacc7b13cfddff2b7cf41317_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_2f65bc41960f3b04069eb1dfaad92fa65eea72356699b9dc298fe6699c5e10a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2f65bc41960f3b04069eb1dfaad92fa65eea72356699b9dc298fe6699c5e10a7->enter($__internal_2f65bc41960f3b04069eb1dfaad92fa65eea72356699b9dc298fe6699c5e10a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_2f65bc41960f3b04069eb1dfaad92fa65eea72356699b9dc298fe6699c5e10a7->leave($__internal_2f65bc41960f3b04069eb1dfaad92fa65eea72356699b9dc298fe6699c5e10a7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:list.html.twig", "/var/www/parkway/api/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list.html.twig");
    }
}
