<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_868eeaabc55e66f4da5785b6309a84814af43526ba937816dfe60642c5312e04 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a2c5521dc127810168638d76d366af501a916820d9f389c27d0baa3ae17e648 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a2c5521dc127810168638d76d366af501a916820d9f389c27d0baa3ae17e648->enter($__internal_8a2c5521dc127810168638d76d366af501a916820d9f389c27d0baa3ae17e648_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_8a2c5521dc127810168638d76d366af501a916820d9f389c27d0baa3ae17e648->leave($__internal_8a2c5521dc127810168638d76d366af501a916820d9f389c27d0baa3ae17e648_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
