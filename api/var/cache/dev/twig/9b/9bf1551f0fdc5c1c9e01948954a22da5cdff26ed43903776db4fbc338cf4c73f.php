<?php

/* EDUBundle:departments:layout_new_department.html.twig */
class __TwigTemplate_6cd534b84a9fe1b1cd85018ec2f8ebc31dd0d975da5dbe2937fd791dcdef7939 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EDUBundle::layout.html.twig", "EDUBundle:departments:layout_new_department.html.twig", 1);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EDUBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6186ec94165b79eefeefdb5f2d4d4870fb47f1d9f5f8b07d1644686469e3781e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6186ec94165b79eefeefdb5f2d4d4870fb47f1d9f5f8b07d1644686469e3781e->enter($__internal_6186ec94165b79eefeefdb5f2d4d4870fb47f1d9f5f8b07d1644686469e3781e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:departments:layout_new_department.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6186ec94165b79eefeefdb5f2d4d4870fb47f1d9f5f8b07d1644686469e3781e->leave($__internal_6186ec94165b79eefeefdb5f2d4d4870fb47f1d9f5f8b07d1644686469e3781e_prof);

    }

    // line 3
    public function block_cmi_body($context, array $blocks = array())
    {
        $__internal_112814883bb642aac872d49de4e3e9a00a23e3beec05ac3944e9f4f4baf0a8dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_112814883bb642aac872d49de4e3e9a00a23e3beec05ac3944e9f4f4baf0a8dc->enter($__internal_112814883bb642aac872d49de4e3e9a00a23e3beec05ac3944e9f4f4baf0a8dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "cmi_body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 6, $this->getSourceContext()); })()), "html", null, true);
        echo "/departments/\" class=\"btn btn-success btn-xs\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Back to List"), "html", null, true);
        echo "</a>
            <br /><br />
</div>
        <div class=\"col-md-2\">
            
        </div>
        <div class=\"col-md-6\">
    ";
        // line 13
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 13, $this->getSourceContext()); })()), 'form_start');
        echo "
        ";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 14, $this->getSourceContext()); })()), 'widget');
        echo "
    ";
        // line 15
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 15, $this->getSourceContext()); })()), 'form_end');
        echo "

        </div>
    </div>
";
        
        $__internal_112814883bb642aac872d49de4e3e9a00a23e3beec05ac3944e9f4f4baf0a8dc->leave($__internal_112814883bb642aac872d49de4e3e9a00a23e3beec05ac3944e9f4f4baf0a8dc_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:departments:layout_new_department.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 15,  60 => 14,  56 => 13,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EDUBundle::layout.html.twig\" %}
    
{% block cmi_body %}
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"{{ base_url}}/departments/\" class=\"btn btn-success btn-xs\">{{ 'Back to List'|trans }}</a>
            <br /><br />
</div>
        <div class=\"col-md-2\">
            
        </div>
        <div class=\"col-md-6\">
    {{ form_start(form) }}
        {{ form_widget(form) }}
    {{ form_end(form) }}

        </div>
    </div>
{% endblock %}
", "EDUBundle:departments:layout_new_department.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/departments/layout_new_department.html.twig");
    }
}
