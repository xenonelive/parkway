<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_dbb2b73a99e2cacb87bfd145653ceb86adc6b0ce750e7578eb50ea61f3dd2a77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_51004a8724b18a3ba67efc1a6264d211f22f0f6efe106ef9ed58e90ceb020d1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51004a8724b18a3ba67efc1a6264d211f22f0f6efe106ef9ed58e90ceb020d1c->enter($__internal_51004a8724b18a3ba67efc1a6264d211f22f0f6efe106ef9ed58e90ceb020d1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_51004a8724b18a3ba67efc1a6264d211f22f0f6efe106ef9ed58e90ceb020d1c->leave($__internal_51004a8724b18a3ba67efc1a6264d211f22f0f6efe106ef9ed58e90ceb020d1c_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_3c8a21c1c1ce249ed7400368aaed633539996e567d08a1f824a8ee257b9c1c2a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c8a21c1c1ce249ed7400368aaed633539996e567d08a1f824a8ee257b9c1c2a->enter($__internal_3c8a21c1c1ce249ed7400368aaed633539996e567d08a1f824a8ee257b9c1c2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_3c8a21c1c1ce249ed7400368aaed633539996e567d08a1f824a8ee257b9c1c2a->leave($__internal_3c8a21c1c1ce249ed7400368aaed633539996e567d08a1f824a8ee257b9c1c2a_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_e257c8a6240ce5888883992737d530bd21651783aca1b7775fadd1816cdadd90 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e257c8a6240ce5888883992737d530bd21651783aca1b7775fadd1816cdadd90->enter($__internal_e257c8a6240ce5888883992737d530bd21651783aca1b7775fadd1816cdadd90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 8, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo ")
";
        
        $__internal_e257c8a6240ce5888883992737d530bd21651783aca1b7775fadd1816cdadd90->leave($__internal_e257c8a6240ce5888883992737d530bd21651783aca1b7775fadd1816cdadd90_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_ed944b82fc2892d03d22eab6c990cc9a0376925c2d4e739b5c615810b1f201e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed944b82fc2892d03d22eab6c990cc9a0376925c2d4e739b5c615810b1f201e0->enter($__internal_ed944b82fc2892d03d22eab6c990cc9a0376925c2d4e739b5c615810b1f201e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_ed944b82fc2892d03d22eab6c990cc9a0376925c2d4e739b5c615810b1f201e0->leave($__internal_ed944b82fc2892d03d22eab6c990cc9a0376925c2d4e739b5c615810b1f201e0_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
