<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_6d5276096f68857e8edb8f1e1daf2db602d3c50052bc0efb54b15bb687c95326 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_415f883857171292796aeee081491a1f26cf0bbcdfaf4b7642783ff19df33299 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_415f883857171292796aeee081491a1f26cf0bbcdfaf4b7642783ff19df33299->enter($__internal_415f883857171292796aeee081491a1f26cf0bbcdfaf4b7642783ff19df33299_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_415f883857171292796aeee081491a1f26cf0bbcdfaf4b7642783ff19df33299->leave($__internal_415f883857171292796aeee081491a1f26cf0bbcdfaf4b7642783ff19df33299_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_47c880fe14c19879c57169abad884d79f38f47aecbd4bcadfc043f2e51a33bca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_47c880fe14c19879c57169abad884d79f38f47aecbd4bcadfc043f2e51a33bca->enter($__internal_47c880fe14c19879c57169abad884d79f38f47aecbd4bcadfc043f2e51a33bca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_47c880fe14c19879c57169abad884d79f38f47aecbd4bcadfc043f2e51a33bca->leave($__internal_47c880fe14c19879c57169abad884d79f38f47aecbd4bcadfc043f2e51a33bca_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "/var/www/parkway/api/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}
