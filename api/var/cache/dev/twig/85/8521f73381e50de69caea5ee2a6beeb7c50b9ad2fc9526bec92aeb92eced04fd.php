<?php

/* EDUBundle:departments:layout_edit_department.html.twig */
class __TwigTemplate_80a8ddc385ae89f45fea5635ebf15761b8a16011bc1b77643780378e8433df24 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EDUBundle::layout.html.twig", "EDUBundle:departments:layout_edit_department.html.twig", 1);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EDUBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ccebd69c906d7ec35b01582f7e4f4e1b12ba3758981e7d063b0125070c99ec66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ccebd69c906d7ec35b01582f7e4f4e1b12ba3758981e7d063b0125070c99ec66->enter($__internal_ccebd69c906d7ec35b01582f7e4f4e1b12ba3758981e7d063b0125070c99ec66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:departments:layout_edit_department.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ccebd69c906d7ec35b01582f7e4f4e1b12ba3758981e7d063b0125070c99ec66->leave($__internal_ccebd69c906d7ec35b01582f7e4f4e1b12ba3758981e7d063b0125070c99ec66_prof);

    }

    // line 3
    public function block_cmi_body($context, array $blocks = array())
    {
        $__internal_c1316a9f4b83e66d0bc60018c98b97473f21be2533c31791dde5490f15105834 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1316a9f4b83e66d0bc60018c98b97473f21be2533c31791dde5490f15105834->enter($__internal_c1316a9f4b83e66d0bc60018c98b97473f21be2533c31791dde5490f15105834_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "cmi_body"));

        // line 4
        echo "    <div class=\"row\">

        <div class=\"col-lg-12\">
            <a href=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 7, $this->getSourceContext()); })()), "html", null, true);
        echo "/departments/\" class=\"btn btn-success btn-xs\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Back to List"), "html", null, true);
        echo "</a>
            </div>
        <div class=\"col-md-2\">
        </div>
        <div class=\"col-md-6\">
            ";
        // line 12
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["edit_form"]) || array_key_exists("edit_form", $context) ? $context["edit_form"] : (function () { throw new Twig_Error_Runtime('Variable "edit_form" does not exist.', 12, $this->getSourceContext()); })()), 'form_start');
        echo "
            ";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["edit_form"]) || array_key_exists("edit_form", $context) ? $context["edit_form"] : (function () { throw new Twig_Error_Runtime('Variable "edit_form" does not exist.', 13, $this->getSourceContext()); })()), 'widget');
        echo "
            ";
        // line 14
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["edit_form"]) || array_key_exists("edit_form", $context) ? $context["edit_form"] : (function () { throw new Twig_Error_Runtime('Variable "edit_form" does not exist.', 14, $this->getSourceContext()); })()), 'form_end');
        echo "
        </div>
    </div>
";
        
        $__internal_c1316a9f4b83e66d0bc60018c98b97473f21be2533c31791dde5490f15105834->leave($__internal_c1316a9f4b83e66d0bc60018c98b97473f21be2533c31791dde5490f15105834_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:departments:layout_edit_department.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 14,  59 => 13,  55 => 12,  45 => 7,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EDUBundle::layout.html.twig\" %}

{% block cmi_body %}
    <div class=\"row\">

        <div class=\"col-lg-12\">
            <a href=\"{{ base_url}}/departments/\" class=\"btn btn-success btn-xs\">{{ 'Back to List'|trans }}</a>
            </div>
        <div class=\"col-md-2\">
        </div>
        <div class=\"col-md-6\">
            {{ form_start(edit_form) }}
            {{ form_widget(edit_form) }}
            {{ form_end(edit_form) }}
        </div>
    </div>
{% endblock %}
", "EDUBundle:departments:layout_edit_department.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/departments/layout_edit_department.html.twig");
    }
}
