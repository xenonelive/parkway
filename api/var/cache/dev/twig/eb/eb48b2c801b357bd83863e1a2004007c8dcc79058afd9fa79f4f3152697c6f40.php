<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_7343871441c3ee8be040a911c386160bd6f357c3acf400f2dee9e335fcf13c98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f91ed19f2e7ee72d54ec4fb6197e8deeee09eea8d636bed8b8f3ad10fbae0a94 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f91ed19f2e7ee72d54ec4fb6197e8deeee09eea8d636bed8b8f3ad10fbae0a94->enter($__internal_f91ed19f2e7ee72d54ec4fb6197e8deeee09eea8d636bed8b8f3ad10fbae0a94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_f91ed19f2e7ee72d54ec4fb6197e8deeee09eea8d636bed8b8f3ad10fbae0a94->leave($__internal_f91ed19f2e7ee72d54ec4fb6197e8deeee09eea8d636bed8b8f3ad10fbae0a94_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
", "@Framework/Form/attributes.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}
