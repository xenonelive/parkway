<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_3a40b2b3766473a5ddee61e2eb2eb46664359e2462266f3388057c59815ba27c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_43a07dd42db34e35431dde1128d811f3258aa78245ba03ab57dfbd442f893aeb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43a07dd42db34e35431dde1128d811f3258aa78245ba03ab57dfbd442f893aeb->enter($__internal_43a07dd42db34e35431dde1128d811f3258aa78245ba03ab57dfbd442f893aeb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_43a07dd42db34e35431dde1128d811f3258aa78245ba03ab57dfbd442f893aeb->leave($__internal_43a07dd42db34e35431dde1128d811f3258aa78245ba03ab57dfbd442f893aeb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
