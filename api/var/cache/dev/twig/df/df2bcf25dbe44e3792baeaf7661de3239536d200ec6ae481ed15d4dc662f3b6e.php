<?php

/* EDUBundle:departments:layout_show_department.html.twig */
class __TwigTemplate_62d8d9170c033e21f2ecbe3caaafb40fe61e618deebb1ea20d584d1ba654e73d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EDUBundle::layout.html.twig", "EDUBundle:departments:layout_show_department.html.twig", 1);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EDUBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_953611ce6b8292fdba364975a256d6b71dd64ca1c85fc6f96b7aed806deb9e66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_953611ce6b8292fdba364975a256d6b71dd64ca1c85fc6f96b7aed806deb9e66->enter($__internal_953611ce6b8292fdba364975a256d6b71dd64ca1c85fc6f96b7aed806deb9e66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:departments:layout_show_department.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_953611ce6b8292fdba364975a256d6b71dd64ca1c85fc6f96b7aed806deb9e66->leave($__internal_953611ce6b8292fdba364975a256d6b71dd64ca1c85fc6f96b7aed806deb9e66_prof);

    }

    // line 3
    public function block_cmi_body($context, array $blocks = array())
    {
        $__internal_d63a1b36b590c902bfecd3c5025423db04dda4885c1892acb9dfb55ff71a54ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d63a1b36b590c902bfecd3c5025423db04dda4885c1892acb9dfb55ff71a54ec->enter($__internal_d63a1b36b590c902bfecd3c5025423db04dda4885c1892acb9dfb55ff71a54ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "cmi_body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 6, $this->getSourceContext()); })()), "html", null, true);
        echo "/departments/\" class=\"btn btn-success
btn-xs\">";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Back to List"), "html", null, true);
        echo "</a>
            <br /><br />
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Id"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Name"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Actions"), "html", null, true);
        echo "</th>
                            </tr>
                        </thead>
                        <tbody>
                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            ";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["department"]) || array_key_exists("department", $context) ? $context["department"] : (function () { throw new Twig_Error_Runtime('Variable "department" does not exist.', 22, $this->getSourceContext()); })()), "id", array()), "html", null, true);
        echo "
                                        </td>
                                        <td>
                                            ";
        // line 25
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["department"]) || array_key_exists("department", $context) ? $context["department"] : (function () { throw new Twig_Error_Runtime('Variable "department" does not exist.', 25, $this->getSourceContext()); })()), "name", array()), "html", null, true);
        echo "
                                        </td>
                                        <td>
                                            
                                            <a href=\"";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 29, $this->getSourceContext()); })()), "html", null, true);
        echo "/departments/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["department"]) || array_key_exists("department", $context) ? $context["department"] : (function () { throw new Twig_Error_Runtime('Variable "department" does not exist.', 29, $this->getSourceContext()); })()), "id", array()), "html", null, true);
        echo "/edit\" class=\"btn btn-xs btn-default\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
        echo "</a>

            ";
        // line 31
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () { throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 31, $this->getSourceContext()); })()), 'form_start');
        echo "
                <input class=\"btn btn-xs btn-danger\" type=\"submit\" value=\"Delete\">
            ";
        // line 33
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () { throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 33, $this->getSourceContext()); })()), 'form_end');
        echo "

                                        </td>
                                    </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
";
        
        $__internal_d63a1b36b590c902bfecd3c5025423db04dda4885c1892acb9dfb55ff71a54ec->leave($__internal_d63a1b36b590c902bfecd3c5025423db04dda4885c1892acb9dfb55ff71a54ec_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:departments:layout_show_department.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 33,  97 => 31,  88 => 29,  81 => 25,  75 => 22,  66 => 16,  62 => 15,  58 => 14,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EDUBundle::layout.html.twig\" %}

{% block cmi_body %}
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"{{ base_url}}/departments/\" class=\"btn btn-success
btn-xs\">{{ 'Back to List'|trans }}</a>
            <br /><br />
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Id'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Name'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Actions'|trans }}</th>
                            </tr>
                        </thead>
                        <tbody>
                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            {{ department.id }}
                                        </td>
                                        <td>
                                            {{ department.name }}
                                        </td>
                                        <td>
                                            
                                            <a href=\"{{ base_url }}/departments/{{ department.id }}/edit\" class=\"btn btn-xs btn-default\">{{ 'Edit'|trans }}</a>

            {{ form_start(delete_form) }}
                <input class=\"btn btn-xs btn-danger\" type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}

                                        </td>
                                    </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
{% endblock %}
", "EDUBundle:departments:layout_show_department.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/departments/layout_show_department.html.twig");
    }
}
