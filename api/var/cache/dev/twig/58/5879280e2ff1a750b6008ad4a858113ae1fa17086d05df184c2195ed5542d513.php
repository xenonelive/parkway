<?php

/* ::base.html.twig */
class __TwigTemplate_a06ca0635eb214cf273cd78363f4788a0712669af19878470a103f7361044ec2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f3ff4f7622f0965cc60a45b4f3b4e0e4f2a66a15a589c74c879592a104b3e68b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3ff4f7622f0965cc60a45b4f3b4e0e4f2a66a15a589c74c879592a104b3e68b->enter($__internal_f3ff4f7622f0965cc60a45b4f3b4e0e4f2a66a15a589c74c879592a104b3e68b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_f3ff4f7622f0965cc60a45b4f3b4e0e4f2a66a15a589c74c879592a104b3e68b->leave($__internal_f3ff4f7622f0965cc60a45b4f3b4e0e4f2a66a15a589c74c879592a104b3e68b_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_c3e028e9184a3f9f01c246813b6ea6957ac74886cf1dfc6cd0b0f322889dbeed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c3e028e9184a3f9f01c246813b6ea6957ac74886cf1dfc6cd0b0f322889dbeed->enter($__internal_c3e028e9184a3f9f01c246813b6ea6957ac74886cf1dfc6cd0b0f322889dbeed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_c3e028e9184a3f9f01c246813b6ea6957ac74886cf1dfc6cd0b0f322889dbeed->leave($__internal_c3e028e9184a3f9f01c246813b6ea6957ac74886cf1dfc6cd0b0f322889dbeed_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f6c84865f6437b81e01818a7e27aaa0b711a29593210bfd4405a32422559f497 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f6c84865f6437b81e01818a7e27aaa0b711a29593210bfd4405a32422559f497->enter($__internal_f6c84865f6437b81e01818a7e27aaa0b711a29593210bfd4405a32422559f497_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_f6c84865f6437b81e01818a7e27aaa0b711a29593210bfd4405a32422559f497->leave($__internal_f6c84865f6437b81e01818a7e27aaa0b711a29593210bfd4405a32422559f497_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_b195b9adc459c18a95038159ef5105bdd1cba90027797ac53806fa43eee096c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b195b9adc459c18a95038159ef5105bdd1cba90027797ac53806fa43eee096c7->enter($__internal_b195b9adc459c18a95038159ef5105bdd1cba90027797ac53806fa43eee096c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_b195b9adc459c18a95038159ef5105bdd1cba90027797ac53806fa43eee096c7->leave($__internal_b195b9adc459c18a95038159ef5105bdd1cba90027797ac53806fa43eee096c7_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_c8297c91b2eb6c84336aa215b26fda54812c63f90dfbc23a14b2ccb196c43293 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8297c91b2eb6c84336aa215b26fda54812c63f90dfbc23a14b2ccb196c43293->enter($__internal_c8297c91b2eb6c84336aa215b26fda54812c63f90dfbc23a14b2ccb196c43293_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_c8297c91b2eb6c84336aa215b26fda54812c63f90dfbc23a14b2ccb196c43293->leave($__internal_c8297c91b2eb6c84336aa215b26fda54812c63f90dfbc23a14b2ccb196c43293_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "/var/www/parkway/api/app/Resources/views/base.html.twig");
    }
}
