<?php

/* EDUBundle:departments:layout_all_departments.html.twig */
class __TwigTemplate_e22e57161219960700fb4e3badbe6973d8ccf30eeafcbbaaf643e7d8b58bbfee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EDUBundle::layout.html.twig", "EDUBundle:departments:layout_all_departments.html.twig", 1);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EDUBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0518e8526e215668fcfb1c8a0e18a0d4258ce359fd1eeae74262093e250bf8f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0518e8526e215668fcfb1c8a0e18a0d4258ce359fd1eeae74262093e250bf8f9->enter($__internal_0518e8526e215668fcfb1c8a0e18a0d4258ce359fd1eeae74262093e250bf8f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:departments:layout_all_departments.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0518e8526e215668fcfb1c8a0e18a0d4258ce359fd1eeae74262093e250bf8f9->leave($__internal_0518e8526e215668fcfb1c8a0e18a0d4258ce359fd1eeae74262093e250bf8f9_prof);

    }

    // line 3
    public function block_cmi_body($context, array $blocks = array())
    {
        $__internal_4ef66494e06ea6d22aff06ab0a82ae64571a4e20380a7380b8c3bdd4b496c900 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ef66494e06ea6d22aff06ab0a82ae64571a4e20380a7380b8c3bdd4b496c900->enter($__internal_4ef66494e06ea6d22aff06ab0a82ae64571a4e20380a7380b8c3bdd4b496c900_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "cmi_body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 6, $this->getSourceContext()); })()), "html", null, true);
        echo "/departments/new\" class=\"btn btn-success
btn-xs\">";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Add New Department"), "html", null, true);
        echo "</a>
            <br /><br />
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Id"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Name"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Actions"), "html", null, true);
        echo "</th>
                            </tr>
                        </thead>
                        <tbody>
                        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["departments"]) || array_key_exists("departments", $context) ? $context["departments"] : (function () { throw new Twig_Error_Runtime('Variable "departments" does not exist.', 20, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["department"]) {
            // line 21
            echo "                                    <tr class=\"gradeA odd\" role=\"row\"
data-id=\"";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()), "html", null, true);
            echo "\" ";
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "getParentid", array())) ? ((("data-parent=" . twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(),             // line 23
$context["department"], "getParentid", array()), "id", array())) . " data-level=2")) : ("data-level=1 ")), "html", null, true);
            echo " >
                                        <td data-column=\"name\"  >
                                            ";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()), "html", null, true);
            echo "
                                        </td>
                                        <td  >
                                            ";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "name", array()), "html", null, true);
            echo "
                                        </td>
                                        <td>
                                            <a href=\"";
            // line 31
            echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 31, $this->getSourceContext()); })()), "html", null, true);
            echo "/departments/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()), "html", null, true);
            echo "\" class=\"btn btn-xs btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("View"), "html", null, true);
            echo "</a>
                                            <a href=\"";
            // line 32
            echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 32, $this->getSourceContext()); })()), "html", null, true);
            echo "/departments/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()), "html", null, true);
            echo "/edit\" class=\"btn btn-xs btn-default\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
            echo "</a>
                                            <a href=\"";
            // line 33
            echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 33, $this->getSourceContext()); })()), "html", null, true);
            // line 34
            echo "/departments/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()), "html", null, true);
            echo "\" class=\"btn btn-xs btn-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Delete"), "html", null, true);
            echo "</a>

                                        </td>
                                    </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['department'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
";
        
        $__internal_4ef66494e06ea6d22aff06ab0a82ae64571a4e20380a7380b8c3bdd4b496c900->leave($__internal_4ef66494e06ea6d22aff06ab0a82ae64571a4e20380a7380b8c3bdd4b496c900_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:departments:layout_all_departments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 39,  118 => 34,  116 => 33,  108 => 32,  100 => 31,  94 => 28,  88 => 25,  83 => 23,  80 => 22,  77 => 21,  73 => 20,  66 => 16,  62 => 15,  58 => 14,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EDUBundle::layout.html.twig\" %}

{% block cmi_body %}
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"{{ base_url}}/departments/new\" class=\"btn btn-success
btn-xs\">{{ 'Add New Department'|trans }}</a>
            <br /><br />
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Id'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Name'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Actions'|trans }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        {% for department in departments %}
                                    <tr class=\"gradeA odd\" role=\"row\"
data-id=\"{{ department.id }}\" {{ (department.getParentid)
? \"data-parent=#{department.getParentid.id} data-level=2\" : 'data-level=1 '  }} >
                                        <td data-column=\"name\"  >
                                            {{ department.id }}
                                        </td>
                                        <td  >
                                            {{ department.name }}
                                        </td>
                                        <td>
                                            <a href=\"{{ base_url }}/departments/{{ department.id }}\" class=\"btn btn-xs btn-primary\">{{ 'View'|trans }}</a>
                                            <a href=\"{{ base_url }}/departments/{{ department.id }}/edit\" class=\"btn btn-xs btn-default\">{{ 'Edit'|trans }}</a>
                                            <a href=\"{{ base_url
}}/departments/{{ department.id }}\" class=\"btn btn-xs btn-danger\">{{ 'Delete'|trans }}</a>

                                        </td>
                                    </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
{% endblock %}
", "EDUBundle:departments:layout_all_departments.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/departments/layout_all_departments.html.twig");
    }
}
