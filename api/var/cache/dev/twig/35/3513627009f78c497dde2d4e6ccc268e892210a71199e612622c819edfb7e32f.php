<?php

/* JMSTranslationBundle::base.html.twig */
class __TwigTemplate_fb34dac15eb9dfa9c155b2828bbd193c4731fbd0add579d3051966481ffba8b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'topjavascripts' => array($this, 'block_topjavascripts'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9dd3edff159b7739390f7333d8d7bd4efa6fa34de50ffff85d9419467b22d448 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9dd3edff159b7739390f7333d8d7bd4efa6fa34de50ffff85d9419467b22d448->enter($__internal_9dd3edff159b7739390f7333d8d7bd4efa6fa34de50ffff85d9419467b22d448_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "JMSTranslationBundle::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/jmstranslation/css/bootstrap.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/jmstranslation/css/layout.css"), "html", null, true);
        echo "\" />
        <link rel=\"shortcut icon\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        ";
        // line 9
        $this->displayBlock('topjavascripts', $context, $blocks);
        // line 10
        echo "    </head>
    <body>
        <div class=\"topbar\">
            <div class=\"topbar-inner\">
                <div class=\"container\">
                    <h3><a href=\"";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("jms_translation_index");
        echo "\" class=\"logo\">JMSTranslationBundle UI</a></h3>
                    
                </div>
            </div>
        </div>
        <div class=\"container\">
            ";
        // line 21
        $this->displayBlock('body', $context, $blocks);
        // line 22
        echo "        </div>

        ";
        // line 24
        $this->displayBlock('javascripts', $context, $blocks);
        // line 29
        echo "    </body>
</html>
";
        
        $__internal_9dd3edff159b7739390f7333d8d7bd4efa6fa34de50ffff85d9419467b22d448->leave($__internal_9dd3edff159b7739390f7333d8d7bd4efa6fa34de50ffff85d9419467b22d448_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_1cd0ee55e3d7167dda36303b47ead44885fd0f15a188f9669c6656e1408befa5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1cd0ee55e3d7167dda36303b47ead44885fd0f15a188f9669c6656e1408befa5->enter($__internal_1cd0ee55e3d7167dda36303b47ead44885fd0f15a188f9669c6656e1408befa5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "JMSTranslationBundle UI";
        
        $__internal_1cd0ee55e3d7167dda36303b47ead44885fd0f15a188f9669c6656e1408befa5->leave($__internal_1cd0ee55e3d7167dda36303b47ead44885fd0f15a188f9669c6656e1408befa5_prof);

    }

    // line 9
    public function block_topjavascripts($context, array $blocks = array())
    {
        $__internal_726921e6dc71cbc9cf2b225cc6337e26287fe41825f076da7fbf7cace08d4320 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_726921e6dc71cbc9cf2b225cc6337e26287fe41825f076da7fbf7cace08d4320->enter($__internal_726921e6dc71cbc9cf2b225cc6337e26287fe41825f076da7fbf7cace08d4320_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "topjavascripts"));

        
        $__internal_726921e6dc71cbc9cf2b225cc6337e26287fe41825f076da7fbf7cace08d4320->leave($__internal_726921e6dc71cbc9cf2b225cc6337e26287fe41825f076da7fbf7cace08d4320_prof);

    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        $__internal_92602d642ee528adf14c603ccbf492b8dfe0b6d5005e67b018929d2c9a736d9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92602d642ee528adf14c603ccbf492b8dfe0b6d5005e67b018929d2c9a736d9b->enter($__internal_92602d642ee528adf14c603ccbf492b8dfe0b6d5005e67b018929d2c9a736d9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_92602d642ee528adf14c603ccbf492b8dfe0b6d5005e67b018929d2c9a736d9b->leave($__internal_92602d642ee528adf14c603ccbf492b8dfe0b6d5005e67b018929d2c9a736d9b_prof);

    }

    // line 24
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_43c5a24dc7fd90b2b2198ce0b52c7cf7b1c2b8b9f2c4a42356f12d5313a8d781 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43c5a24dc7fd90b2b2198ce0b52c7cf7b1c2b8b9f2c4a42356f12d5313a8d781->enter($__internal_43c5a24dc7fd90b2b2198ce0b52c7cf7b1c2b8b9f2c4a42356f12d5313a8d781_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 25
        echo "        <script language=\"javascript\" type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/jmstranslation/js/jquery.js"), "html", null, true);
        echo "\"></script>
        <script language=\"javascript\" type=\"text/javascript\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/jmstranslation/js/trunk8.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/jmstranslation/js/jms.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_43c5a24dc7fd90b2b2198ce0b52c7cf7b1c2b8b9f2c4a42356f12d5313a8d781->leave($__internal_43c5a24dc7fd90b2b2198ce0b52c7cf7b1c2b8b9f2c4a42356f12d5313a8d781_prof);

    }

    public function getTemplateName()
    {
        return "JMSTranslationBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 27,  128 => 26,  123 => 25,  117 => 24,  106 => 21,  95 => 9,  83 => 5,  74 => 29,  72 => 24,  68 => 22,  66 => 21,  57 => 15,  50 => 10,  48 => 9,  44 => 8,  40 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <title>{% block title %}JMSTranslationBundle UI{% endblock %}</title>
        <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"{{ asset(\"bundles/jmstranslation/css/bootstrap.css\") }}\" />
        <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"{{ asset(\"bundles/jmstranslation/css/layout.css\") }}\" />
        <link rel=\"shortcut icon\" href=\"{{ asset('favicon.ico') }}\" />
        {% block topjavascripts %}{% endblock %}
    </head>
    <body>
        <div class=\"topbar\">
            <div class=\"topbar-inner\">
                <div class=\"container\">
                    <h3><a href=\"{{ path(\"jms_translation_index\") }}\" class=\"logo\">JMSTranslationBundle UI</a></h3>
                    
                </div>
            </div>
        </div>
        <div class=\"container\">
            {% block body %}{% endblock %}
        </div>

        {% block javascripts %}
        <script language=\"javascript\" type=\"text/javascript\" src=\"{{ asset(\"bundles/jmstranslation/js/jquery.js\") }}\"></script>
        <script language=\"javascript\" type=\"text/javascript\" src=\"{{ asset(\"bundles/jmstranslation/js/trunk8.js\") }}\"></script>
            <script type=\"text/javascript\" src=\"{{ asset(\"bundles/jmstranslation/js/jms.js\") }}\"></script>
        {% endblock %}
    </body>
</html>
", "JMSTranslationBundle::base.html.twig", "/var/www/parkway/api/vendor/jms/translation-bundle/JMS/TranslationBundle/Resources/views/base.html.twig");
    }
}
