<?php

/* :departments:show.html.twig */
class __TwigTemplate_f92e22cd199a7986c166afa4fbe00cdbf1f7273c4632b66148e48a49ea0baf52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":departments:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_59d639576f3cbddfa3790365f8f88bced874f04c7c554628de4ae5b53e9e242f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59d639576f3cbddfa3790365f8f88bced874f04c7c554628de4ae5b53e9e242f->enter($__internal_59d639576f3cbddfa3790365f8f88bced874f04c7c554628de4ae5b53e9e242f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":departments:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_59d639576f3cbddfa3790365f8f88bced874f04c7c554628de4ae5b53e9e242f->leave($__internal_59d639576f3cbddfa3790365f8f88bced874f04c7c554628de4ae5b53e9e242f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_1a26763aa165c54e0bb0bfd57b44663b52fe64c739735685fe51a0d4d4ccddc2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a26763aa165c54e0bb0bfd57b44663b52fe64c739735685fe51a0d4d4ccddc2->enter($__internal_1a26763aa165c54e0bb0bfd57b44663b52fe64c739735685fe51a0d4d4ccddc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Department</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["department"]) || array_key_exists("department", $context) ? $context["department"] : (function () { throw new Twig_Error_Runtime('Variable "department" does not exist.', 10, $this->getSourceContext()); })()), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["department"]) || array_key_exists("department", $context) ? $context["department"] : (function () { throw new Twig_Error_Runtime('Variable "department" does not exist.', 14, $this->getSourceContext()); })()), "name", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("departments_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("departments_edit", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["department"]) || array_key_exists("department", $context) ? $context["department"] : (function () { throw new Twig_Error_Runtime('Variable "department" does not exist.', 24, $this->getSourceContext()); })()), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 27
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () { throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 27, $this->getSourceContext()); })()), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 29
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () { throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 29, $this->getSourceContext()); })()), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_1a26763aa165c54e0bb0bfd57b44663b52fe64c739735685fe51a0d4d4ccddc2->leave($__internal_1a26763aa165c54e0bb0bfd57b44663b52fe64c739735685fe51a0d4d4ccddc2_prof);

    }

    public function getTemplateName()
    {
        return ":departments:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 29,  77 => 27,  71 => 24,  65 => 21,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Department</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ department.id }}</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>{{ department.name }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('departments_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('departments_edit', { 'id': department.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":departments:show.html.twig", "/var/www/parkway/api/app/Resources/views/departments/show.html.twig");
    }
}
