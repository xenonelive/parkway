<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_03f7c2324e035b7bb48fa1b8470776f2b61c2e9507218ed311b27951a124be74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cdf310f40e345ac27899571d7a7d453c7c5bf16f9f5f182c901853700d7e4311 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cdf310f40e345ac27899571d7a7d453c7c5bf16f9f5f182c901853700d7e4311->enter($__internal_cdf310f40e345ac27899571d7a7d453c7c5bf16f9f5f182c901853700d7e4311_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_cdf310f40e345ac27899571d7a7d453c7c5bf16f9f5f182c901853700d7e4311->leave($__internal_cdf310f40e345ac27899571d7a7d453c7c5bf16f9f5f182c901853700d7e4311_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_f9ce86d527922ab8c820575749f62fa05bd365d2e4a78aff77272fe0e2705b6e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f9ce86d527922ab8c820575749f62fa05bd365d2e4a78aff77272fe0e2705b6e->enter($__internal_f9ce86d527922ab8c820575749f62fa05bd365d2e4a78aff77272fe0e2705b6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_f9ce86d527922ab8c820575749f62fa05bd365d2e4a78aff77272fe0e2705b6e->leave($__internal_f9ce86d527922ab8c820575749f62fa05bd365d2e4a78aff77272fe0e2705b6e_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
