<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_a9bd728f3bdbdc9a34c2ef66a6957ad697839985b892999ca9029308e2f6f78e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_37648a7d71d7f5afe39ee70bfda1ffc9c90971ff72ff665d0b7bf967afb97a36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37648a7d71d7f5afe39ee70bfda1ffc9c90971ff72ff665d0b7bf967afb97a36->enter($__internal_37648a7d71d7f5afe39ee70bfda1ffc9c90971ff72ff665d0b7bf967afb97a36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_37648a7d71d7f5afe39ee70bfda1ffc9c90971ff72ff665d0b7bf967afb97a36->leave($__internal_37648a7d71d7f5afe39ee70bfda1ffc9c90971ff72ff665d0b7bf967afb97a36_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_e13444834b8f17bb431742edbb241d681086c043b26ec1830446a01724929dae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e13444834b8f17bb431742edbb241d681086c043b26ec1830446a01724929dae->enter($__internal_e13444834b8f17bb431742edbb241d681086c043b26ec1830446a01724929dae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_e13444834b8f17bb431742edbb241d681086c043b26ec1830446a01724929dae->leave($__internal_e13444834b8f17bb431742edbb241d681086c043b26ec1830446a01724929dae_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "/var/www/parkway/api/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
