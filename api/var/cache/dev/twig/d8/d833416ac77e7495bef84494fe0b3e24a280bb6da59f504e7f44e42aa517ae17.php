<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_34086647f2b2f20f8dee9da993d945926630c5485259c92291bc5a06d3d273cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e20adc156f122fdf0ec682243c5d681718a152cdc29b102f8c18bb62891556ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e20adc156f122fdf0ec682243c5d681718a152cdc29b102f8c18bb62891556ec->enter($__internal_e20adc156f122fdf0ec682243c5d681718a152cdc29b102f8c18bb62891556ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_e20adc156f122fdf0ec682243c5d681718a152cdc29b102f8c18bb62891556ec->leave($__internal_e20adc156f122fdf0ec682243c5d681718a152cdc29b102f8c18bb62891556ec_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
