<?php

/* EDUBundle:Default/email:registration.html.twig */
class __TwigTemplate_8635f73edb1b3e36cfd042c34e0acc73bd743ce201fc8457874526c8f66d603a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body_text' => array($this, 'block_body_text'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1ee6016b95f35d98a9169b50f0e971e873c179bdffa909e26f79e4a3205ce7e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ee6016b95f35d98a9169b50f0e971e873c179bdffa909e26f79e4a3205ce7e6->enter($__internal_1ee6016b95f35d98a9169b50f0e971e873c179bdffa909e26f79e4a3205ce7e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:Default/email:registration.html.twig"));

        // line 1
        $this->displayBlock('body_text', $context, $blocks);
        
        $__internal_1ee6016b95f35d98a9169b50f0e971e873c179bdffa909e26f79e4a3205ce7e6->leave($__internal_1ee6016b95f35d98a9169b50f0e971e873c179bdffa909e26f79e4a3205ce7e6_prof);

    }

    public function block_body_text($context, array $blocks = array())
    {
        $__internal_7dcba85d6bc2013762e514e41e5edf51c26c9f39e3af84a68ccccbc39ef2eb32 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7dcba85d6bc2013762e514e41e5edf51c26c9f39e3af84a68ccccbc39ef2eb32->enter($__internal_7dcba85d6bc2013762e514e41e5edf51c26c9f39e3af84a68ccccbc39ef2eb32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 2
        echo "    ";
        // line 3
        echo "        Hello ";
        echo twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 3, $this->getSourceContext()); })()), "username", array());
        echo " !
        <br/>
    ";
        
        $__internal_7dcba85d6bc2013762e514e41e5edf51c26c9f39e3af84a68ccccbc39ef2eb32->leave($__internal_7dcba85d6bc2013762e514e41e5edf51c26c9f39e3af84a68ccccbc39ef2eb32_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:Default/email:registration.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 3,  35 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block body_text %}
    {% autoescape false %}
        Hello {{ user.username }} !
        <br/>
    {% endautoescape %}
{% endblock %}
", "EDUBundle:Default/email:registration.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/Default/email/registration.html.twig");
    }
}
