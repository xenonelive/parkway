<?php

/* EDUBundle:users:layout_all_users.html.twig */
class __TwigTemplate_b33e3d270f897009b5a75e9cf8ffbb8e2a60345cedc5633a6c5b9abad4ff29ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EDUBundle::layout.html.twig", "EDUBundle:users:layout_all_users.html.twig", 1);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EDUBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_befa60e11272a2878faecad8ff8ecbdcbed19dafc4ab95de80aed6905321d2a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_befa60e11272a2878faecad8ff8ecbdcbed19dafc4ab95de80aed6905321d2a8->enter($__internal_befa60e11272a2878faecad8ff8ecbdcbed19dafc4ab95de80aed6905321d2a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:users:layout_all_users.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_befa60e11272a2878faecad8ff8ecbdcbed19dafc4ab95de80aed6905321d2a8->leave($__internal_befa60e11272a2878faecad8ff8ecbdcbed19dafc4ab95de80aed6905321d2a8_prof);

    }

    // line 3
    public function block_cmi_body($context, array $blocks = array())
    {
        $__internal_08cb680ab672c10fc37ea73c73d9a2a4e3f630de812f793f6cfe80d3dc241449 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08cb680ab672c10fc37ea73c73d9a2a4e3f630de812f793f6cfe80d3dc241449->enter($__internal_08cb680ab672c10fc37ea73c73d9a2a4e3f630de812f793f6cfe80d3dc241449_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "cmi_body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 6, $this->getSourceContext()); })()), "html", null, true);
        echo "/user/add\" class=\"btn btn-success btn-xs\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Add New User"), "html", null, true);
        echo "</a>
            <br /><br />
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Name"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Email"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Department"), "html", null, true);
        // line 16
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Actions"), "html", null, true);
        echo "</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Name"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Email"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Department"), "html", null, true);
        // line 25
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Actions"), "html", null, true);
        echo "</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 30, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 31
            echo "                            ";
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "enabled", array())) {
                // line 32
                echo "                                ";
                if (((null === twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "updated", array())) ||  !twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "updated", array()))) {
                    // line 33
                    echo "                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            ";
                    // line 35
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastname", array()), "html", null, true);
                    echo "
                                        </td>
                                        <td>
                                            ";
                    // line 38
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
                    echo "
                                        </td>
                                        <td>
                                           ";
                    // line 41
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "department", array()), "html", null, true);
                    echo "  
                                        </td>
                                        <td>
                                            
                                            <input type=\"buton\" class=\"btn btn-xs btn-primary\" style=\"width:69px;\" value=\"";
                    // line 45
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("View"), "html", null, true);
                    echo "\"  onclick=\"detail(";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo ")\">
                                            </input>  
                                            
                                            
                                            <a href=\"";
                    // line 49
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 49, $this->getSourceContext()); })()), "html", null, true);
                    echo "/user/edit/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-default\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
                    echo "</a>
                                            <a href=\"";
                    // line 50
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 50, $this->getSourceContext()); })()), "html", null, true);
                    echo "/user/delete/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-danger\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Delete"), "html", null, true);
                    echo "</a>
                                        </td>
                                    </tr>
                                ";
                } else {
                    // line 54
                    echo "                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            <b>";
                    // line 56
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastname", array()), "html", null, true);
                    echo "</b>
                                        </td>
                                        <td>
                                            <b>";
                    // line 59
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
                    echo "</b>
                                        </td>
                                        <td>
                                            <b>";
                    // line 62
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "phoneNumber", array()), "html", null, true);
                    echo "</b>
                                        </td>
                                        <td>
                                           
                                              <input type=\"buton\" class=\"btn btn-xs btn-primary\" style=\"width:69px;\" value=\"";
                    // line 66
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("View"), "html", null, true);
                    echo "\"  onclick=\"detail(";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo ")\">
                                             </input>  
                                            
                                            
                                            <a href=\"";
                    // line 70
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 70, $this->getSourceContext()); })()), "html", null, true);
                    echo "/user/edit/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-default\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
                    echo "</a>
                                            <a href=\"";
                    // line 71
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 71, $this->getSourceContext()); })()), "html", null, true);
                    echo "/user/delete/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-danger\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Delete"), "html", null, true);
                    echo "</a>
                                        </td>
                                    </tr>
                                ";
                }
                // line 75
                echo "                            ";
            }
            // line 76
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!--moodle-->


  ";
        // line 86
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 86, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 87
            echo "    ";
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "enabled", array())) {
                // line 88
                echo "    ";
                if (((null === twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "updated", array())) ||  !twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "updated", array()))) {
                    echo "   
    
    
    <div class=\"container\">
  <!-- Modal -->
  <div class=\"modal fade\" id=\"detail_";
                    // line 93
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" role=\"dialog\" tabindex=\"-1\">
    
  
    <div class=\"modal-dialog\" role=\"document\">
      <!-- Modal content-->
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;
          </button>
          <h4 class=\"modal-title\"><b>";
                    // line 102
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user-all-popup-heading"), "html", null, true);
                    echo "</b>
          </h4>
        </div>
        <div class=\"modal-body\">
          <!--<a href=\"";
                    // line 106
                    echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_teachers");
                    echo "\" class=\"btn btn-sm btn-primary\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Go Back"), "html", null, true);
                    echo "
          </a>-->
           <a href=\"";
                    // line 108
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 108, $this->getSourceContext()); })()), "html", null, true);
                    echo "/user/edit/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-sm btn-default\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
                    echo " </a>
          </a>
          <br/>
          <br/>
          <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
             <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <tbody>
                            <tr class=\"gradeA odd\" role=\"row\">
                                <td>
                                    ";
                    // line 117
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Username"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 120
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
                    echo " 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 125
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Name"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 128
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastname", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 133
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Email"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 136
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <!-- new-->
                            <tr>
                                <td>
                                    ";
                    // line 142
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Role"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                
                                ";
                    // line 146
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "getRoles", array(), "method"));
                    foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                        // line 147
                        echo "                                      
                   
                                        ";
                        // line 149
                        if (($context["user"] == "ROLE_ADMIN")) {
                            // line 150
                            echo "                                               <span> ";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_admin"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 152
                        echo "                                        ";
                        if (($context["user"] == "EMPLOYEE")) {
                            // line 153
                            echo "                                               <span> ";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("EMPLOYEE"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 155
                        echo "                                     
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 157
                    echo "                                      
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    ";
                    // line 163
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Department"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                     ";
                    // line 166
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "department", array()), "html", null, true);
                    echo " 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 171
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Language"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 174
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "fr")) {
                        // line 175
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_france"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 177
                    echo "                                    ";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "nl")) {
                        // line 178
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_netherland"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 180
                    echo "                                     ";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "en")) {
                        // line 181
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_english"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 183
                    echo "                                    
                                    
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    ";
                    // line 189
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Address"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 192
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "address", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    ";
                    // line 197
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Postcode"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 200
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "postCode", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                              
                                <tr>
                                <td>
                                    ";
                    // line 206
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-conatct_number"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 209
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "contactNumber", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                        
                            <!--  -->
                            <tr>
                                <td>
                                    ";
                    // line 216
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-phoneNumber"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 219
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "phoneNumber", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 224
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-lastLogin"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 227
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastlogin", array()), "Y-m-d H:i:s"), "html", null, true);
                    echo "
                                </td>
                            </tr>
                        </tbody>
                    </table>
        </div>
        <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
    ";
                } else {
                    // line 242
                    echo "         <div class=\"container\">
  <!-- Modal -->
  <div class=\"modal fade\" id=\"detail_";
                    // line 244
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" role=\"dialog\" tabindex=\"-1\">
    
  
    <div class=\"modal-dialog\" role=\"document\">
      <!-- Modal content-->
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;
          </button>
          <h4 class=\"modal-title\"><b>";
                    // line 253
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user-all-popup-heading"), "html", null, true);
                    echo "</b>
          </h4>
        </div>
        <div class=\"modal-body\">
          <!--<a href=\"";
                    // line 257
                    echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_teachers");
                    echo "\" class=\"btn btn-sm btn-primary\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Go Back"), "html", null, true);
                    echo "
          </a>-->
           <a href=\"";
                    // line 259
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 259, $this->getSourceContext()); })()), "html", null, true);
                    echo "/user/edit/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-sm btn-default\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
                    echo " </a>
          </a>
          <br/>
          <br/>
          <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
             <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <tbody>
                            <tr class=\"gradeA odd\" role=\"row\">
                                <td>
                                    ";
                    // line 268
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Username"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 271
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
                    echo " 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 276
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Name"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 279
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastname", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 284
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Email"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 287
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    ";
                    // line 292
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Role"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                   ";
                    // line 295
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "getRoles", array(), "method"));
                    foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                        // line 296
                        echo "                                      
                                        ";
                        // line 297
                        if (($context["user"] == "ROLE_ADMIN")) {
                            // line 298
                            echo "                                               <span> ";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_admin"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 300
                        echo "                                        ";
                        if (($context["user"] == "ROLE_PARENT")) {
                            // line 301
                            echo "                                               <span> ";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_parent"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 303
                        echo "                                        ";
                        if (($context["user"] == "ROLE_TEACHER")) {
                            echo "          
                                               <span> ";
                            // line 304
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_teacher"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 306
                        echo "                                        ";
                        if (($context["user"] == "ROLE_STUDENT")) {
                            echo "          
                                               <span> ";
                            // line 307
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_student"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 309
                        echo "                                   
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 311
                    echo "                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 315
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Language"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 318
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "fr")) {
                        // line 319
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_france"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 321
                    echo "                                    ";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "nl")) {
                        // line 322
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_netherland"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 324
                    echo "                                    ";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "en")) {
                        // line 325
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_english"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 327
                    echo "                                    
                                    
                            
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    ";
                    // line 334
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Address"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 337
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "address", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    ";
                    // line 342
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Postcode"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 345
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "postCode", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                              
                                <tr>
                                <td>
                                    ";
                    // line 351
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-conatct_number"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 354
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "contactNumber", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                                
                            <tr>
                                <td>
                                    ";
                    // line 360
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.phoneNumber"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 363
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "phoneNumber", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 368
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-lastLogin"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 371
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastlogin", array()), "Y-m-d H:i:s"), "html", null, true);
                    echo "
                                </td>
                            </tr>
                        </tbody>
                    </table>
        </div>
        <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
   
";
                }
                // line 387
                echo " 
";
            }
            // line 389
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 390
        echo "  
    
";
        
        $__internal_08cb680ab672c10fc37ea73c73d9a2a4e3f630de812f793f6cfe80d3dc241449->leave($__internal_08cb680ab672c10fc37ea73c73d9a2a4e3f630de812f793f6cfe80d3dc241449_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:users:layout_all_users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  812 => 390,  805 => 389,  801 => 387,  782 => 371,  776 => 368,  768 => 363,  762 => 360,  753 => 354,  747 => 351,  738 => 345,  732 => 342,  724 => 337,  718 => 334,  709 => 327,  703 => 325,  700 => 324,  694 => 322,  691 => 321,  685 => 319,  683 => 318,  677 => 315,  671 => 311,  664 => 309,  659 => 307,  654 => 306,  649 => 304,  644 => 303,  638 => 301,  635 => 300,  629 => 298,  627 => 297,  624 => 296,  620 => 295,  614 => 292,  606 => 287,  600 => 284,  590 => 279,  584 => 276,  576 => 271,  570 => 268,  552 => 259,  545 => 257,  538 => 253,  526 => 244,  522 => 242,  504 => 227,  498 => 224,  490 => 219,  484 => 216,  474 => 209,  468 => 206,  459 => 200,  453 => 197,  445 => 192,  439 => 189,  431 => 183,  425 => 181,  422 => 180,  416 => 178,  413 => 177,  407 => 175,  405 => 174,  399 => 171,  391 => 166,  385 => 163,  377 => 157,  370 => 155,  364 => 153,  361 => 152,  355 => 150,  353 => 149,  349 => 147,  345 => 146,  338 => 142,  329 => 136,  323 => 133,  313 => 128,  307 => 125,  299 => 120,  293 => 117,  275 => 108,  268 => 106,  261 => 102,  249 => 93,  240 => 88,  237 => 87,  233 => 86,  222 => 77,  216 => 76,  213 => 75,  202 => 71,  194 => 70,  185 => 66,  178 => 62,  172 => 59,  164 => 56,  160 => 54,  149 => 50,  141 => 49,  132 => 45,  125 => 41,  119 => 38,  111 => 35,  107 => 33,  104 => 32,  101 => 31,  97 => 30,  90 => 26,  87 => 25,  85 => 24,  81 => 23,  77 => 22,  69 => 17,  66 => 16,  64 => 15,  60 => 14,  56 => 13,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EDUBundle::layout.html.twig\" %}

{% block cmi_body %}
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"{{ base_url}}/user/add\" class=\"btn btn-success btn-xs\">{{ 'Add New User'|trans }}</a>
            <br /><br />
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Name'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Email'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{
'Department'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Actions'|trans }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Name'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Email'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{
'Department'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Actions'|trans }}</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        {%  for user in users %}
                            {% if user.enabled %}
                                {% if user.updated is null or not user.updated %}
                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            {{ user.firstname }} {{ user.lastname }}
                                        </td>
                                        <td>
                                            {{ user.email }}
                                        </td>
                                        <td>
                                           {{ user.department }}  
                                        </td>
                                        <td>
                                            
                                            <input type=\"buton\" class=\"btn btn-xs btn-primary\" style=\"width:69px;\" value=\"{{ 'View'|trans }}\"  onclick=\"detail({{ user.id }})\">
                                            </input>  
                                            
                                            
                                            <a href=\"{{ base_url }}/user/edit/{{ user.id }}\" class=\"btn btn-xs btn-default\">{{ 'Edit'|trans }}</a>
                                            <a href=\"{{ base_url }}/user/delete/{{ user.id }}\" class=\"btn btn-xs btn-danger\">{{ 'Delete'|trans }}</a>
                                        </td>
                                    </tr>
                                {% else %}
                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            <b>{{ user.firstname }} {{ user.lastname }}</b>
                                        </td>
                                        <td>
                                            <b>{{ user.email }}</b>
                                        </td>
                                        <td>
                                            <b>{{ user.phoneNumber }}</b>
                                        </td>
                                        <td>
                                           
                                              <input type=\"buton\" class=\"btn btn-xs btn-primary\" style=\"width:69px;\" value=\"{{ 'View'|trans }}\"  onclick=\"detail({{ user.id }})\">
                                             </input>  
                                            
                                            
                                            <a href=\"{{ base_url }}/user/edit/{{ user.id }}\" class=\"btn btn-xs btn-default\">{{ 'Edit'|trans }}</a>
                                            <a href=\"{{ base_url }}/user/delete/{{ user.id }}\" class=\"btn btn-xs btn-danger\">{{ 'Delete'|trans }}</a>
                                        </td>
                                    </tr>
                                {% endif %}
                            {% endif %}
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!--moodle-->


  {%  for user in users %}
    {% if user.enabled %}
    {% if user.updated is null or not user.updated %}   
    
    
    <div class=\"container\">
  <!-- Modal -->
  <div class=\"modal fade\" id=\"detail_{{ user.id }}\" role=\"dialog\" tabindex=\"-1\">
    
  
    <div class=\"modal-dialog\" role=\"document\">
      <!-- Modal content-->
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;
          </button>
          <h4 class=\"modal-title\"><b>{{ 'user-all-popup-heading'|trans }}</b>
          </h4>
        </div>
        <div class=\"modal-body\">
          <!--<a href=\"{{ path('user_teachers') }}\" class=\"btn btn-sm btn-primary\">{{ 'Go Back'|trans }}
          </a>-->
           <a href=\"{{base_url}}/user/edit/{{ user.id }}\" class=\"btn btn-sm btn-default\">{{ 'Edit'|trans }} {{ user.username }} </a>
          </a>
          <br/>
          <br/>
          <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
             <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <tbody>
                            <tr class=\"gradeA odd\" role=\"row\">
                                <td>
                                    {{ 'allUser-Username'|trans }}
                                </td>
                                <td>
                                    {{ user.username }} 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-Name'|trans }}
                                </td>
                                <td>
                                    {{ user.firstname }} {{ user.lastname }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-Email'|trans }}
                                </td>
                                <td>
                                    {{ user.email }}
                                </td>
                            </tr>
                            <!-- new-->
                            <tr>
                                <td>
                                    {{ 'allUser-Role'|trans }}
                                </td>
                                <td>
                                
                                {% for user in user.getRoles() %}
                                      
                   
                                        {% if user =='ROLE_ADMIN' %}
                                               <span> {{ 'user_admin'|trans }}</span></br>
                                        {% endif %}
                                        {% if user =='EMPLOYEE' %}
                                               <span> {{ 'EMPLOYEE'|trans }}</span></br>
                                        {% endif %}
                                     
                                {% endfor %}
                                      
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    {{ 'Department'|trans }}
                                </td>
                                <td>
                                     {{ user.department }} 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-Language'|trans }}
                                </td>
                                <td>
                                    {% if user.locale =='fr' %}
                                        <span> {{ 'user_language_france'|trans }}</span>
                                    {% endif %}
                                    {% if user.locale =='nl' %}
                                        <span> {{ 'user_language_netherland'|trans }}</span>
                                    {% endif %}
                                     {% if user.locale =='en' %}
                                        <span> {{ 'user_language_english'|trans }}</span>
                                    {% endif %}
                                    
                                    
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    {{ 'allUser-Address'|trans }}
                                </td>
                                <td>
                                    {{ user.contact.address }}
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    {{ 'allUser-Postcode'|trans }}
                                </td>
                                <td>
                                    {{ user.contact.postCode }}
                                </td>
                            </tr>
                              
                                <tr>
                                <td>
                                    {{ 'allUser-conatct_number'|trans }}
                                </td>
                                <td>
                                    {{ user.contact.contactNumber }}
                                </td>
                            </tr>
                        
                            <!--  -->
                            <tr>
                                <td>
                                    {{ 'allUser-phoneNumber'|trans }}
                                </td>
                                <td>
                                    {{ user.phoneNumber }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-lastLogin'|trans }}
                                </td>
                                <td>
                                    {{ user.lastlogin|date('Y-m-d H:i:s') }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
        </div>
        <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
    {% else %}
         <div class=\"container\">
  <!-- Modal -->
  <div class=\"modal fade\" id=\"detail_{{ user.id }}\" role=\"dialog\" tabindex=\"-1\">
    
  
    <div class=\"modal-dialog\" role=\"document\">
      <!-- Modal content-->
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;
          </button>
          <h4 class=\"modal-title\"><b>{{ 'user-all-popup-heading'|trans }}</b>
          </h4>
        </div>
        <div class=\"modal-body\">
          <!--<a href=\"{{ path('user_teachers') }}\" class=\"btn btn-sm btn-primary\">{{ 'Go Back'|trans }}
          </a>-->
           <a href=\"{{base_url}}/user/edit/{{ user.id }}\" class=\"btn btn-sm btn-default\">{{ 'Edit'|trans }} {{ user.username }} </a>
          </a>
          <br/>
          <br/>
          <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
             <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <tbody>
                            <tr class=\"gradeA odd\" role=\"row\">
                                <td>
                                    {{ 'allUser-Username'|trans }}
                                </td>
                                <td>
                                    {{ user.username }} 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-Name'|trans }}
                                </td>
                                <td>
                                    {{ user.firstname }} {{ user.lastname }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-Email'|trans }}
                                </td>
                                <td>
                                    {{ user.email }}
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    {{ 'allUser-Role'|trans }}
                                </td>
                                <td>
                                   {% for user in user.getRoles() %}
                                      
                                        {% if user =='ROLE_ADMIN' %}
                                               <span> {{ 'user_admin'|trans }}</span></br>
                                        {% endif %}
                                        {% if user =='ROLE_PARENT' %}
                                               <span> {{ 'user_parent'|trans }}</span></br>
                                        {% endif %}
                                        {% if user =='ROLE_TEACHER' %}          
                                               <span> {{ 'user_teacher'|trans }}</span></br>
                                        {% endif %}
                                        {% if user =='ROLE_STUDENT' %}          
                                               <span> {{ 'user_student'|trans }}</span></br>
                                        {% endif %}
                                   
                                {% endfor %}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-Language'|trans }}
                                </td>
                                <td>
                                    {% if user.locale =='fr' %}
                                        <span> {{ 'user_language_france'|trans }}</span>
                                    {% endif %}
                                    {% if user.locale =='nl' %}
                                        <span> {{ 'user_language_netherland'|trans }}</span>
                                    {% endif %}
                                    {% if user.locale =='en' %}
                                        <span> {{ 'user_language_english'|trans }}</span>
                                    {% endif %}
                                    
                                    
                            
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    {{ 'allUser-Address'|trans }}
                                </td>
                                <td>
                                    {{ user.contact.address }}
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    {{ 'allUser-Postcode'|trans }}
                                </td>
                                <td>
                                    {{ user.contact.postCode }}
                                </td>
                            </tr>
                              
                                <tr>
                                <td>
                                    {{ 'allUser-conatct_number'|trans }}
                                </td>
                                <td>
                                    {{ user.contact.contactNumber }}
                                </td>
                            </tr>
                                
                            <tr>
                                <td>
                                    {{ 'user.phoneNumber'|trans }}
                                </td>
                                <td>
                                    {{ user.phoneNumber }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-lastLogin'|trans }}
                                </td>
                                <td>
                                    {{ user.lastlogin|date('Y-m-d H:i:s') }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
        </div>
        <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
   
{% endif %}
 
{% endif %}

    {% endfor %}  
    
{% endblock %}
", "EDUBundle:users:layout_all_users.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/users/layout_all_users.html.twig");
    }
}
