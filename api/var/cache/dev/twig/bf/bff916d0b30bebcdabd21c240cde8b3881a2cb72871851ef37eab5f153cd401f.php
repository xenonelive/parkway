<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_78c627ab3881c72190f7772311ebbfca92755ee6024dd4b74af8b4e0b6a3c308 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_542d2cb5d329d99ee01e34ad67969e2668156d28391c653a129ec6d80a977075 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_542d2cb5d329d99ee01e34ad67969e2668156d28391c653a129ec6d80a977075->enter($__internal_542d2cb5d329d99ee01e34ad67969e2668156d28391c653a129ec6d80a977075_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_542d2cb5d329d99ee01e34ad67969e2668156d28391c653a129ec6d80a977075->leave($__internal_542d2cb5d329d99ee01e34ad67969e2668156d28391c653a129ec6d80a977075_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_82206ad21e9323dc1f45a85d5b597d8055119a2911c3e42a383777d689707992 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_82206ad21e9323dc1f45a85d5b597d8055119a2911c3e42a383777d689707992->enter($__internal_82206ad21e9323dc1f45a85d5b597d8055119a2911c3e42a383777d689707992_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_82206ad21e9323dc1f45a85d5b597d8055119a2911c3e42a383777d689707992->leave($__internal_82206ad21e9323dc1f45a85d5b597d8055119a2911c3e42a383777d689707992_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:register.html.twig", "/var/www/parkway/api/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
