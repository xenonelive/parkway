<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_6d1628e379bd993c34058916508eb13d981be6daa10036ea94d4f66ea507169c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8dd9e8cb807d031d1a7e6e5f15fd38adaf8527eb4559b6e87b724349762ed77d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8dd9e8cb807d031d1a7e6e5f15fd38adaf8527eb4559b6e87b724349762ed77d->enter($__internal_8dd9e8cb807d031d1a7e6e5f15fd38adaf8527eb4559b6e87b724349762ed77d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_8dd9e8cb807d031d1a7e6e5f15fd38adaf8527eb4559b6e87b724349762ed77d->leave($__internal_8dd9e8cb807d031d1a7e6e5f15fd38adaf8527eb4559b6e87b724349762ed77d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
