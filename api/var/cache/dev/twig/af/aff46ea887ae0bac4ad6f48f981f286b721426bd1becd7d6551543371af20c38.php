<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_48502014ea5ad49aa176a1e82cbb2dbbf94d15ff6778c4f62fc1778fb2e316ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e5758462cc8966e38ab9a591619a64283acc3dfcb08e722e85b8b64550247f3f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5758462cc8966e38ab9a591619a64283acc3dfcb08e722e85b8b64550247f3f->enter($__internal_e5758462cc8966e38ab9a591619a64283acc3dfcb08e722e85b8b64550247f3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e5758462cc8966e38ab9a591619a64283acc3dfcb08e722e85b8b64550247f3f->leave($__internal_e5758462cc8966e38ab9a591619a64283acc3dfcb08e722e85b8b64550247f3f_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_1d27bdd7799aaca3ebac9db166442bc5cee69b59bb9b2240475c3f93ce80b7db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d27bdd7799aaca3ebac9db166442bc5cee69b59bb9b2240475c3f93ce80b7db->enter($__internal_1d27bdd7799aaca3ebac9db166442bc5cee69b59bb9b2240475c3f93ce80b7db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_1d27bdd7799aaca3ebac9db166442bc5cee69b59bb9b2240475c3f93ce80b7db->leave($__internal_1d27bdd7799aaca3ebac9db166442bc5cee69b59bb9b2240475c3f93ce80b7db_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_b50ca8c250b66c5877b8fac6d3372140980fcd98d039b43ba24ffdffe6b29977 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b50ca8c250b66c5877b8fac6d3372140980fcd98d039b43ba24ffdffe6b29977->enter($__internal_b50ca8c250b66c5877b8fac6d3372140980fcd98d039b43ba24ffdffe6b29977_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_b50ca8c250b66c5877b8fac6d3372140980fcd98d039b43ba24ffdffe6b29977->leave($__internal_b50ca8c250b66c5877b8fac6d3372140980fcd98d039b43ba24ffdffe6b29977_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_73e2adf6c950129695f58e90eb556464cd4ec535e7be84e3cf52086bc21a06ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_73e2adf6c950129695f58e90eb556464cd4ec535e7be84e3cf52086bc21a06ae->enter($__internal_73e2adf6c950129695f58e90eb556464cd4ec535e7be84e3cf52086bc21a06ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_73e2adf6c950129695f58e90eb556464cd4ec535e7be84e3cf52086bc21a06ae->leave($__internal_73e2adf6c950129695f58e90eb556464cd4ec535e7be84e3cf52086bc21a06ae_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
