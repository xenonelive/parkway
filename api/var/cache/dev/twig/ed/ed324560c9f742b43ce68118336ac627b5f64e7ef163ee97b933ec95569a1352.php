<?php

/* EDUBundle:users:layout_all_users.html.twig */
class __TwigTemplate_0a1b55586e8cc94952fea1f696ea07cc53e6e2c0a1822cfa7f4991da8de7304b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EDUBundle::layout.html.twig", "EDUBundle:users:layout_all_users.html.twig", 1);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EDUBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c0915bd9a410146c023f286338ea9946e01610ce242511bc9248a494e89fac95 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c0915bd9a410146c023f286338ea9946e01610ce242511bc9248a494e89fac95->enter($__internal_c0915bd9a410146c023f286338ea9946e01610ce242511bc9248a494e89fac95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:users:layout_all_users.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c0915bd9a410146c023f286338ea9946e01610ce242511bc9248a494e89fac95->leave($__internal_c0915bd9a410146c023f286338ea9946e01610ce242511bc9248a494e89fac95_prof);

    }

    // line 3
    public function block_cmi_body($context, array $blocks = array())
    {
        $__internal_83984469d85b6e3d32070a3a1d4767a4bcd4efa48f3b25b2679cecb22a35a27e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83984469d85b6e3d32070a3a1d4767a4bcd4efa48f3b25b2679cecb22a35a27e->enter($__internal_83984469d85b6e3d32070a3a1d4767a4bcd4efa48f3b25b2679cecb22a35a27e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "cmi_body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 6, $this->getSourceContext()); })()), "html", null, true);
        echo "/user/add\" class=\"btn btn-success btn-xs\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Add New User"), "html", null, true);
        echo "</a>
            <br /><br />
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Name"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Email"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Department"), "html", null, true);
        // line 16
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Actions"), "html", null, true);
        echo "</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Name"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Email"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Department"), "html", null, true);
        // line 25
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Actions"), "html", null, true);
        echo "</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 30, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 31
            echo "                            ";
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "enabled", array())) {
                // line 32
                echo "                                ";
                if (((null === twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "updated", array())) ||  !twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "updated", array()))) {
                    // line 33
                    echo "                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            ";
                    // line 35
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastname", array()), "html", null, true);
                    echo "
                                        </td>
                                        <td>
                                            ";
                    // line 38
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
                    echo "
                                        </td>
                                        <td>
                                           ";
                    // line 41
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "department", array()), "html", null, true);
                    echo "  
                                        </td>
                                        <td>
                                            
                                            <input type=\"buton\" class=\"btn btn-xs btn-primary\" style=\"width:69px;\" value=\"";
                    // line 45
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("View"), "html", null, true);
                    echo "\"  onclick=\"detail(";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo ")\">
                                            </input>  
                                            
                                            
                                            <a href=\"";
                    // line 49
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 49, $this->getSourceContext()); })()), "html", null, true);
                    echo "/user/edit/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-default\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
                    echo "</a>
                                            <a href=\"";
                    // line 50
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 50, $this->getSourceContext()); })()), "html", null, true);
                    echo "/user/delete/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-danger\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Delete"), "html", null, true);
                    echo "</a>
                                        </td>
                                    </tr>
                                ";
                } else {
                    // line 54
                    echo "                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            <b>";
                    // line 56
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastname", array()), "html", null, true);
                    echo "</b>
                                        </td>
                                        <td>
                                            <b>";
                    // line 59
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
                    echo "</b>
                                        </td>
                                        <td>
                                            <b>";
                    // line 62
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "phoneNumber", array()), "html", null, true);
                    echo "</b>
                                        </td>
                                        <td>
                                           
                                              <input type=\"buton\" class=\"btn btn-xs btn-primary\" style=\"width:69px;\" value=\"";
                    // line 66
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("View"), "html", null, true);
                    echo "\"  onclick=\"detail(";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo ")\">
                                             </input>  
                                            
                                            
                                            <a href=\"";
                    // line 70
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 70, $this->getSourceContext()); })()), "html", null, true);
                    echo "/user/edit/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-default\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
                    echo "</a>
                                            <a href=\"";
                    // line 71
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 71, $this->getSourceContext()); })()), "html", null, true);
                    echo "/user/delete/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-danger\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Delete"), "html", null, true);
                    echo "</a>
                                        </td>
                                    </tr>
                                ";
                }
                // line 75
                echo "                            ";
            }
            // line 76
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!--moodle-->


  ";
        // line 86
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 86, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 87
            echo "    ";
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "enabled", array())) {
                // line 88
                echo "    ";
                if (((null === twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "updated", array())) ||  !twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "updated", array()))) {
                    echo "   
    
    
    <div class=\"container\">
  <!-- Modal -->
  <div class=\"modal fade\" id=\"detail_";
                    // line 93
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" role=\"dialog\" tabindex=\"-1\">
    
  
    <div class=\"modal-dialog\" role=\"document\">
      <!-- Modal content-->
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;
          </button>
          <h4 class=\"modal-title\"><b>";
                    // line 102
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user-all-popup-heading"), "html", null, true);
                    echo "</b>
          </h4>
        </div>
        <div class=\"modal-body\">
           <a href=\"";
                    // line 106
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 106, $this->getSourceContext()); })()), "html", null, true);
                    echo "/user/edit/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-sm btn-default\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
                    echo " </a>
          </a>
          <br/>
          <br/>
          <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
             <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <tbody>
                            <tr class=\"gradeA odd\" role=\"row\">
                                <td>
                                    ";
                    // line 115
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Username"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 118
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
                    echo " 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 123
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Name"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 126
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastname", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 131
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Email"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 134
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <!-- new-->
                            <tr>
                                <td>
                                    ";
                    // line 140
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Role"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                
                                ";
                    // line 144
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "getRoles", array(), "method"));
                    foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                        // line 145
                        echo "                                      
                   
                                        ";
                        // line 147
                        if (($context["user"] == "ROLE_ADMIN")) {
                            // line 148
                            echo "                                               <span> ";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_admin"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 150
                        echo "                                        ";
                        if (($context["user"] == "EMPLOYEE")) {
                            // line 151
                            echo "                                               <span> ";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("EMPLOYEE"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 153
                        echo "                                     
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 155
                    echo "                                      
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    ";
                    // line 161
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Department"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                     ";
                    // line 164
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "department", array()), "html", null, true);
                    echo " 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 169
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Language"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 172
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "fr")) {
                        // line 173
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_france"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 175
                    echo "                                    ";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "nl")) {
                        // line 176
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_netherland"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 178
                    echo "                                     ";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "en")) {
                        // line 179
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_english"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 181
                    echo "                                    
                                    
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    ";
                    // line 187
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Address"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 190
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "address", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    ";
                    // line 195
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Postcode"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 198
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "postCode", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                              
                                <tr>
                                <td>
                                    ";
                    // line 204
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-conatct_number"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 207
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "contactNumber", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                        
                            <!--  -->
                            <tr>
                                <td>
                                    ";
                    // line 214
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-phoneNumber"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 217
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "phoneNumber", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 222
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-lastLogin"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 225
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastlogin", array()), "Y-m-d H:i:s"), "html", null, true);
                    echo "
                                </td>
                            </tr>
                        </tbody>
                    </table>
        </div>
        <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
    ";
                } else {
                    // line 240
                    echo "         <div class=\"container\">
  <!-- Modal -->
  <div class=\"modal fade\" id=\"detail_";
                    // line 242
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" role=\"dialog\" tabindex=\"-1\">
    
  
    <div class=\"modal-dialog\" role=\"document\">
      <!-- Modal content-->
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;
          </button>
          <h4 class=\"modal-title\"><b>";
                    // line 251
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user-all-popup-heading"), "html", null, true);
                    echo "</b>
          </h4>
        </div>
        <div class=\"modal-body\">
          </a>-->
           <a href=\"";
                    // line 256
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 256, $this->getSourceContext()); })()), "html", null, true);
                    echo "/user/edit/";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"btn btn-sm btn-default\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
                    echo " </a>
          </a>
          <br/>
          <br/>
          <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
             <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <tbody>
                            <tr class=\"gradeA odd\" role=\"row\">
                                <td>
                                    ";
                    // line 265
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Username"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 268
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
                    echo " 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 273
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Name"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 276
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastname", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 281
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Email"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 284
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    ";
                    // line 289
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Role"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                   ";
                    // line 292
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "getRoles", array(), "method"));
                    foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                        // line 293
                        echo "                                      
                                        ";
                        // line 294
                        if (($context["user"] == "ROLE_ADMIN")) {
                            // line 295
                            echo "                                               <span> ";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_admin"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 297
                        echo "                                        ";
                        if (($context["user"] == "ROLE_PARENT")) {
                            // line 298
                            echo "                                               <span> ";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_parent"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 300
                        echo "                                        ";
                        if (($context["user"] == "ROLE_TEACHER")) {
                            echo "          
                                               <span> ";
                            // line 301
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_teacher"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 303
                        echo "                                        ";
                        if (($context["user"] == "ROLE_STUDENT")) {
                            echo "          
                                               <span> ";
                            // line 304
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_student"), "html", null, true);
                            echo "</span></br>
                                        ";
                        }
                        // line 306
                        echo "                                   
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 308
                    echo "                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 312
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Language"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 315
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "fr")) {
                        // line 316
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_france"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 318
                    echo "                                    ";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "nl")) {
                        // line 319
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_netherland"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 321
                    echo "                                    ";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "locale", array()) == "en")) {
                        // line 322
                        echo "                                        <span> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user_language_english"), "html", null, true);
                        echo "</span>
                                    ";
                    }
                    // line 324
                    echo "                                    
                                    
                            
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    ";
                    // line 331
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Address"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 334
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "address", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    ";
                    // line 339
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-Postcode"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 342
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "postCode", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                              
                                <tr>
                                <td>
                                    ";
                    // line 348
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-conatct_number"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 351
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "contact", array()), "contactNumber", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                                
                            <tr>
                                <td>
                                    ";
                    // line 357
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.phoneNumber"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 360
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "phoneNumber", array()), "html", null, true);
                    echo "
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ";
                    // line 365
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allUser-lastLogin"), "html", null, true);
                    echo "
                                </td>
                                <td>
                                    ";
                    // line 368
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "lastlogin", array()), "Y-m-d H:i:s"), "html", null, true);
                    echo "
                                </td>
                            </tr>
                        </tbody>
                    </table>
        </div>
        <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
   
";
                }
                // line 384
                echo " 
";
            }
            // line 386
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 387
        echo "  
    
";
        
        $__internal_83984469d85b6e3d32070a3a1d4767a4bcd4efa48f3b25b2679cecb22a35a27e->leave($__internal_83984469d85b6e3d32070a3a1d4767a4bcd4efa48f3b25b2679cecb22a35a27e_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:users:layout_all_users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  799 => 387,  792 => 386,  788 => 384,  769 => 368,  763 => 365,  755 => 360,  749 => 357,  740 => 351,  734 => 348,  725 => 342,  719 => 339,  711 => 334,  705 => 331,  696 => 324,  690 => 322,  687 => 321,  681 => 319,  678 => 318,  672 => 316,  670 => 315,  664 => 312,  658 => 308,  651 => 306,  646 => 304,  641 => 303,  636 => 301,  631 => 300,  625 => 298,  622 => 297,  616 => 295,  614 => 294,  611 => 293,  607 => 292,  601 => 289,  593 => 284,  587 => 281,  577 => 276,  571 => 273,  563 => 268,  557 => 265,  539 => 256,  531 => 251,  519 => 242,  515 => 240,  497 => 225,  491 => 222,  483 => 217,  477 => 214,  467 => 207,  461 => 204,  452 => 198,  446 => 195,  438 => 190,  432 => 187,  424 => 181,  418 => 179,  415 => 178,  409 => 176,  406 => 175,  400 => 173,  398 => 172,  392 => 169,  384 => 164,  378 => 161,  370 => 155,  363 => 153,  357 => 151,  354 => 150,  348 => 148,  346 => 147,  342 => 145,  338 => 144,  331 => 140,  322 => 134,  316 => 131,  306 => 126,  300 => 123,  292 => 118,  286 => 115,  268 => 106,  261 => 102,  249 => 93,  240 => 88,  237 => 87,  233 => 86,  222 => 77,  216 => 76,  213 => 75,  202 => 71,  194 => 70,  185 => 66,  178 => 62,  172 => 59,  164 => 56,  160 => 54,  149 => 50,  141 => 49,  132 => 45,  125 => 41,  119 => 38,  111 => 35,  107 => 33,  104 => 32,  101 => 31,  97 => 30,  90 => 26,  87 => 25,  85 => 24,  81 => 23,  77 => 22,  69 => 17,  66 => 16,  64 => 15,  60 => 14,  56 => 13,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EDUBundle::layout.html.twig\" %}

{% block cmi_body %}
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"{{ base_url}}/user/add\" class=\"btn btn-success btn-xs\">{{ 'Add New User'|trans }}</a>
            <br /><br />
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Name'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Email'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{
'Department'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Actions'|trans }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Name'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Email'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{
'Department'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Actions'|trans }}</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        {%  for user in users %}
                            {% if user.enabled %}
                                {% if user.updated is null or not user.updated %}
                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            {{ user.firstname }} {{ user.lastname }}
                                        </td>
                                        <td>
                                            {{ user.email }}
                                        </td>
                                        <td>
                                           {{ user.department }}  
                                        </td>
                                        <td>
                                            
                                            <input type=\"buton\" class=\"btn btn-xs btn-primary\" style=\"width:69px;\" value=\"{{ 'View'|trans }}\"  onclick=\"detail({{ user.id }})\">
                                            </input>  
                                            
                                            
                                            <a href=\"{{ base_url }}/user/edit/{{ user.id }}\" class=\"btn btn-xs btn-default\">{{ 'Edit'|trans }}</a>
                                            <a href=\"{{ base_url }}/user/delete/{{ user.id }}\" class=\"btn btn-xs btn-danger\">{{ 'Delete'|trans }}</a>
                                        </td>
                                    </tr>
                                {% else %}
                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            <b>{{ user.firstname }} {{ user.lastname }}</b>
                                        </td>
                                        <td>
                                            <b>{{ user.email }}</b>
                                        </td>
                                        <td>
                                            <b>{{ user.phoneNumber }}</b>
                                        </td>
                                        <td>
                                           
                                              <input type=\"buton\" class=\"btn btn-xs btn-primary\" style=\"width:69px;\" value=\"{{ 'View'|trans }}\"  onclick=\"detail({{ user.id }})\">
                                             </input>  
                                            
                                            
                                            <a href=\"{{ base_url }}/user/edit/{{ user.id }}\" class=\"btn btn-xs btn-default\">{{ 'Edit'|trans }}</a>
                                            <a href=\"{{ base_url }}/user/delete/{{ user.id }}\" class=\"btn btn-xs btn-danger\">{{ 'Delete'|trans }}</a>
                                        </td>
                                    </tr>
                                {% endif %}
                            {% endif %}
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!--moodle-->


  {%  for user in users %}
    {% if user.enabled %}
    {% if user.updated is null or not user.updated %}   
    
    
    <div class=\"container\">
  <!-- Modal -->
  <div class=\"modal fade\" id=\"detail_{{ user.id }}\" role=\"dialog\" tabindex=\"-1\">
    
  
    <div class=\"modal-dialog\" role=\"document\">
      <!-- Modal content-->
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;
          </button>
          <h4 class=\"modal-title\"><b>{{ 'user-all-popup-heading'|trans }}</b>
          </h4>
        </div>
        <div class=\"modal-body\">
           <a href=\"{{base_url}}/user/edit/{{ user.id }}\" class=\"btn btn-sm btn-default\">{{ 'Edit'|trans }} {{ user.username }} </a>
          </a>
          <br/>
          <br/>
          <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
             <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <tbody>
                            <tr class=\"gradeA odd\" role=\"row\">
                                <td>
                                    {{ 'allUser-Username'|trans }}
                                </td>
                                <td>
                                    {{ user.username }} 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-Name'|trans }}
                                </td>
                                <td>
                                    {{ user.firstname }} {{ user.lastname }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-Email'|trans }}
                                </td>
                                <td>
                                    {{ user.email }}
                                </td>
                            </tr>
                            <!-- new-->
                            <tr>
                                <td>
                                    {{ 'allUser-Role'|trans }}
                                </td>
                                <td>
                                
                                {% for user in user.getRoles() %}
                                      
                   
                                        {% if user =='ROLE_ADMIN' %}
                                               <span> {{ 'user_admin'|trans }}</span></br>
                                        {% endif %}
                                        {% if user =='EMPLOYEE' %}
                                               <span> {{ 'EMPLOYEE'|trans }}</span></br>
                                        {% endif %}
                                     
                                {% endfor %}
                                      
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    {{ 'Department'|trans }}
                                </td>
                                <td>
                                     {{ user.department }} 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-Language'|trans }}
                                </td>
                                <td>
                                    {% if user.locale =='fr' %}
                                        <span> {{ 'user_language_france'|trans }}</span>
                                    {% endif %}
                                    {% if user.locale =='nl' %}
                                        <span> {{ 'user_language_netherland'|trans }}</span>
                                    {% endif %}
                                     {% if user.locale =='en' %}
                                        <span> {{ 'user_language_english'|trans }}</span>
                                    {% endif %}
                                    
                                    
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    {{ 'allUser-Address'|trans }}
                                </td>
                                <td>
                                    {{ user.contact.address }}
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    {{ 'allUser-Postcode'|trans }}
                                </td>
                                <td>
                                    {{ user.contact.postCode }}
                                </td>
                            </tr>
                              
                                <tr>
                                <td>
                                    {{ 'allUser-conatct_number'|trans }}
                                </td>
                                <td>
                                    {{ user.contact.contactNumber }}
                                </td>
                            </tr>
                        
                            <!--  -->
                            <tr>
                                <td>
                                    {{ 'allUser-phoneNumber'|trans }}
                                </td>
                                <td>
                                    {{ user.phoneNumber }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-lastLogin'|trans }}
                                </td>
                                <td>
                                    {{ user.lastlogin|date('Y-m-d H:i:s') }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
        </div>
        <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
    {% else %}
         <div class=\"container\">
  <!-- Modal -->
  <div class=\"modal fade\" id=\"detail_{{ user.id }}\" role=\"dialog\" tabindex=\"-1\">
    
  
    <div class=\"modal-dialog\" role=\"document\">
      <!-- Modal content-->
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;
          </button>
          <h4 class=\"modal-title\"><b>{{ 'user-all-popup-heading'|trans }}</b>
          </h4>
        </div>
        <div class=\"modal-body\">
          </a>-->
           <a href=\"{{base_url}}/user/edit/{{ user.id }}\" class=\"btn btn-sm btn-default\">{{ 'Edit'|trans }} {{ user.username }} </a>
          </a>
          <br/>
          <br/>
          <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
             <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <tbody>
                            <tr class=\"gradeA odd\" role=\"row\">
                                <td>
                                    {{ 'allUser-Username'|trans }}
                                </td>
                                <td>
                                    {{ user.username }} 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-Name'|trans }}
                                </td>
                                <td>
                                    {{ user.firstname }} {{ user.lastname }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-Email'|trans }}
                                </td>
                                <td>
                                    {{ user.email }}
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    {{ 'allUser-Role'|trans }}
                                </td>
                                <td>
                                   {% for user in user.getRoles() %}
                                      
                                        {% if user =='ROLE_ADMIN' %}
                                               <span> {{ 'user_admin'|trans }}</span></br>
                                        {% endif %}
                                        {% if user =='ROLE_PARENT' %}
                                               <span> {{ 'user_parent'|trans }}</span></br>
                                        {% endif %}
                                        {% if user =='ROLE_TEACHER' %}          
                                               <span> {{ 'user_teacher'|trans }}</span></br>
                                        {% endif %}
                                        {% if user =='ROLE_STUDENT' %}          
                                               <span> {{ 'user_student'|trans }}</span></br>
                                        {% endif %}
                                   
                                {% endfor %}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-Language'|trans }}
                                </td>
                                <td>
                                    {% if user.locale =='fr' %}
                                        <span> {{ 'user_language_france'|trans }}</span>
                                    {% endif %}
                                    {% if user.locale =='nl' %}
                                        <span> {{ 'user_language_netherland'|trans }}</span>
                                    {% endif %}
                                    {% if user.locale =='en' %}
                                        <span> {{ 'user_language_english'|trans }}</span>
                                    {% endif %}
                                    
                                    
                            
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    {{ 'allUser-Address'|trans }}
                                </td>
                                <td>
                                    {{ user.contact.address }}
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    {{ 'allUser-Postcode'|trans }}
                                </td>
                                <td>
                                    {{ user.contact.postCode }}
                                </td>
                            </tr>
                              
                                <tr>
                                <td>
                                    {{ 'allUser-conatct_number'|trans }}
                                </td>
                                <td>
                                    {{ user.contact.contactNumber }}
                                </td>
                            </tr>
                                
                            <tr>
                                <td>
                                    {{ 'user.phoneNumber'|trans }}
                                </td>
                                <td>
                                    {{ user.phoneNumber }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ 'allUser-lastLogin'|trans }}
                                </td>
                                <td>
                                    {{ user.lastlogin|date('Y-m-d H:i:s') }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
        </div>
        <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
   
{% endif %}
 
{% endif %}

    {% endfor %}  
    
{% endblock %}
", "EDUBundle:users:layout_all_users.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/users/layout_all_users.html.twig");
    }
}
