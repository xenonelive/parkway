<?php

/* EDUBundle:class:layout_new_class.html.twig */
class __TwigTemplate_cb6418f381fe403d81659f2edd77460208245923df423986123e2c5663210494 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EDUBundle::layout.html.twig", "EDUBundle:class:layout_new_class.html.twig", 1);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EDUBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1e0aa271ed3cba1a9b36c65335ac9967dfa13a362525d434bb3e77e5361bd5a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e0aa271ed3cba1a9b36c65335ac9967dfa13a362525d434bb3e77e5361bd5a5->enter($__internal_1e0aa271ed3cba1a9b36c65335ac9967dfa13a362525d434bb3e77e5361bd5a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:class:layout_new_class.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1e0aa271ed3cba1a9b36c65335ac9967dfa13a362525d434bb3e77e5361bd5a5->leave($__internal_1e0aa271ed3cba1a9b36c65335ac9967dfa13a362525d434bb3e77e5361bd5a5_prof);

    }

    // line 3
    public function block_cmi_body($context, array $blocks = array())
    {
        $__internal_61e76ab75658a2f6b143fc359790d7557ebdddb26a38c41b3080ba4ea29ad93d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61e76ab75658a2f6b143fc359790d7557ebdddb26a38c41b3080ba4ea29ad93d->enter($__internal_61e76ab75658a2f6b143fc359790d7557ebdddb26a38c41b3080ba4ea29ad93d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "cmi_body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-md-2\">
            
        </div>
        <div class=\"col-md-6\">
            ";
        // line 9
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext()); })()), 'form_start');
        echo "
            ";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 10, $this->getSourceContext()); })()), 'widget');
        echo "
            ";
        // line 11
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 11, $this->getSourceContext()); })()), 'form_end');
        echo "
        </div>
    </div>
";
        
        $__internal_61e76ab75658a2f6b143fc359790d7557ebdddb26a38c41b3080ba4ea29ad93d->leave($__internal_61e76ab75658a2f6b143fc359790d7557ebdddb26a38c41b3080ba4ea29ad93d_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:class:layout_new_class.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 11,  51 => 10,  47 => 9,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EDUBundle::layout.html.twig\" %}
    
{% block cmi_body %}
    <div class=\"row\">
        <div class=\"col-md-2\">
            
        </div>
        <div class=\"col-md-6\">
            {{form_start(form)}}
            {{form_widget(form)}}
            {{form_end(form)}}
        </div>
    </div>
{% endblock %}", "EDUBundle:class:layout_new_class.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/class/layout_new_class.html.twig");
    }
}
