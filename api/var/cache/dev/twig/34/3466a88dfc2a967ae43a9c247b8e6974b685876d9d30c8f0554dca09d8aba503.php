<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_6cadb4771cd84761c933a9b88d8272ec10007a1f959da1324b942c51ea5e9f25 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6a3824cc2150f6e9cd0a69428d0d092b35b6b8bc0fa7ec4daa906d34e3c4875b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a3824cc2150f6e9cd0a69428d0d092b35b6b8bc0fa7ec4daa906d34e3c4875b->enter($__internal_6a3824cc2150f6e9cd0a69428d0d092b35b6b8bc0fa7ec4daa906d34e3c4875b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6a3824cc2150f6e9cd0a69428d0d092b35b6b8bc0fa7ec4daa906d34e3c4875b->leave($__internal_6a3824cc2150f6e9cd0a69428d0d092b35b6b8bc0fa7ec4daa906d34e3c4875b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_2a8df3072a2a2f5d72eed17196fc26715da8df9a932f0a0c6070ab19c5fab0d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a8df3072a2a2f5d72eed17196fc26715da8df9a932f0a0c6070ab19c5fab0d9->enter($__internal_2a8df3072a2a2f5d72eed17196fc26715da8df9a932f0a0c6070ab19c5fab0d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_2a8df3072a2a2f5d72eed17196fc26715da8df9a932f0a0c6070ab19c5fab0d9->leave($__internal_2a8df3072a2a2f5d72eed17196fc26715da8df9a932f0a0c6070ab19c5fab0d9_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_0cc5479de831b3783df72db8b76fc29bc4149ae3d70d032b2ef8512936217321 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0cc5479de831b3783df72db8b76fc29bc4149ae3d70d032b2ef8512936217321->enter($__internal_0cc5479de831b3783df72db8b76fc29bc4149ae3d70d032b2ef8512936217321_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_0cc5479de831b3783df72db8b76fc29bc4149ae3d70d032b2ef8512936217321->leave($__internal_0cc5479de831b3783df72db8b76fc29bc4149ae3d70d032b2ef8512936217321_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_de6f760298b23331648b3b860fe87fdf0b9f13d7af66bbcb1b1bd1b109fe3678 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de6f760298b23331648b3b860fe87fdf0b9f13d7af66bbcb1b1bd1b109fe3678->enter($__internal_de6f760298b23331648b3b860fe87fdf0b9f13d7af66bbcb1b1bd1b109fe3678_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_de6f760298b23331648b3b860fe87fdf0b9f13d7af66bbcb1b1bd1b109fe3678->leave($__internal_de6f760298b23331648b3b860fe87fdf0b9f13d7af66bbcb1b1bd1b109fe3678_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
