<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_b5f37ff322feb9c6ba0053b386350ea57e31402dd13b8d3a0414fdbc00b3ed6d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_58264d4e87b91722fc9fa3527ab630f0559e9a19cbbd3be496999581cd293337 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58264d4e87b91722fc9fa3527ab630f0559e9a19cbbd3be496999581cd293337->enter($__internal_58264d4e87b91722fc9fa3527ab630f0559e9a19cbbd3be496999581cd293337_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_58264d4e87b91722fc9fa3527ab630f0559e9a19cbbd3be496999581cd293337->leave($__internal_58264d4e87b91722fc9fa3527ab630f0559e9a19cbbd3be496999581cd293337_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}
