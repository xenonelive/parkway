<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_4a39fc903cf3205149682607f41ea6f2464c2b57e418248ded563edd329fec0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f921e446300413bfd1f704c06f3bed5070e7792e0bfad141893c0a4565e86d26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f921e446300413bfd1f704c06f3bed5070e7792e0bfad141893c0a4565e86d26->enter($__internal_f921e446300413bfd1f704c06f3bed5070e7792e0bfad141893c0a4565e86d26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_f921e446300413bfd1f704c06f3bed5070e7792e0bfad141893c0a4565e86d26->leave($__internal_f921e446300413bfd1f704c06f3bed5070e7792e0bfad141893c0a4565e86d26_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
