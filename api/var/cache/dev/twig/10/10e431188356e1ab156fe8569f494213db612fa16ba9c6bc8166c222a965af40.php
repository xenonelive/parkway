<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_ef09a5bdc3b74b1cbbc37b3cc713c7e41bf30a1d95496f86ed478c6d906cc217 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f3128fce9468896f9568cabb1fa0042cb3a125fb4e495055de4614779d19ef16 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3128fce9468896f9568cabb1fa0042cb3a125fb4e495055de4614779d19ef16->enter($__internal_f3128fce9468896f9568cabb1fa0042cb3a125fb4e495055de4614779d19ef16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f3128fce9468896f9568cabb1fa0042cb3a125fb4e495055de4614779d19ef16->leave($__internal_f3128fce9468896f9568cabb1fa0042cb3a125fb4e495055de4614779d19ef16_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a25e846b2248919f817e8fc22eb59a17358f3eea40039a9d755df242eb98d483 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a25e846b2248919f817e8fc22eb59a17358f3eea40039a9d755df242eb98d483->enter($__internal_a25e846b2248919f817e8fc22eb59a17358f3eea40039a9d755df242eb98d483_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_a25e846b2248919f817e8fc22eb59a17358f3eea40039a9d755df242eb98d483->leave($__internal_a25e846b2248919f817e8fc22eb59a17358f3eea40039a9d755df242eb98d483_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "/var/www/parkway/api/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}
