<?php

/* EDUBundle:Form:newuser.html.twig */
class __TwigTemplate_677888e032066f3aa6cab9144a2915488fb8c6831288859540bf9cb406252986 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EDUBundle::layout.html.twig", "EDUBundle:Form:newuser.html.twig", 1);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EDUBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e164d51e6bf71935432e8b74980e9dbd2753481fac9debd98c8d70ca823ed35b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e164d51e6bf71935432e8b74980e9dbd2753481fac9debd98c8d70ca823ed35b->enter($__internal_e164d51e6bf71935432e8b74980e9dbd2753481fac9debd98c8d70ca823ed35b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:Form:newuser.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e164d51e6bf71935432e8b74980e9dbd2753481fac9debd98c8d70ca823ed35b->leave($__internal_e164d51e6bf71935432e8b74980e9dbd2753481fac9debd98c8d70ca823ed35b_prof);

    }

    // line 3
    public function block_cmi_body($context, array $blocks = array())
    {
        $__internal_5a8df3a0442e82b4ae4c5e9f06ed6ddb09cc657190dcd39dc8a0b79f3024722f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a8df3a0442e82b4ae4c5e9f06ed6ddb09cc657190dcd39dc8a0b79f3024722f->enter($__internal_5a8df3a0442e82b4ae4c5e9f06ed6ddb09cc657190dcd39dc8a0b79f3024722f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "cmi_body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-md-2\">
            
        </div>
        <div class=\"col-md-6\">
            ";
        // line 9
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext()); })()), 'form_start');
        echo "
                ";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 10, $this->getSourceContext()); })()), 'widget');
        echo "
            ";
        // line 11
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 11, $this->getSourceContext()); })()), 'form_end');
        echo "
        </div>
    </div>
";
        
        $__internal_5a8df3a0442e82b4ae4c5e9f06ed6ddb09cc657190dcd39dc8a0b79f3024722f->leave($__internal_5a8df3a0442e82b4ae4c5e9f06ed6ddb09cc657190dcd39dc8a0b79f3024722f_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:Form:newuser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 11,  51 => 10,  47 => 9,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EDUBundle::layout.html.twig\" %}
    
{% block cmi_body %}
    <div class=\"row\">
        <div class=\"col-md-2\">
            
        </div>
        <div class=\"col-md-6\">
            {{form_start(form)}}
                {{form_widget(form)}}
            {{form_end(form)}}
        </div>
    </div>
{% endblock %}", "EDUBundle:Form:newuser.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/Form/newuser.html.twig");
    }
}
