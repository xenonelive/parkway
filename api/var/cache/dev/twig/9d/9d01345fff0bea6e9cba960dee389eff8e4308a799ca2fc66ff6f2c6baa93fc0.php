<?php

/* :departments:index.html.twig */
class __TwigTemplate_ccc167b966c5d54c64090949111c431b4c7cddfa52fa72f4fc33b0839a05f398 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":departments:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c75f7392d737d03d55db7d7db412a27f827f2d689ccb27d48fd627f505543042 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c75f7392d737d03d55db7d7db412a27f827f2d689ccb27d48fd627f505543042->enter($__internal_c75f7392d737d03d55db7d7db412a27f827f2d689ccb27d48fd627f505543042_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":departments:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c75f7392d737d03d55db7d7db412a27f827f2d689ccb27d48fd627f505543042->leave($__internal_c75f7392d737d03d55db7d7db412a27f827f2d689ccb27d48fd627f505543042_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_65515f725494397dda995af5838d413b58512a0a2954d399506787095a2ded87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_65515f725494397dda995af5838d413b58512a0a2954d399506787095a2ded87->enter($__internal_65515f725494397dda995af5838d413b58512a0a2954d399506787095a2ded87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Departments list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["departments"]) || array_key_exists("departments", $context) ? $context["departments"] : (function () { throw new Twig_Error_Runtime('Variable "departments" does not exist.', 15, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["department"]) {
            // line 16
            echo "            <tr>
                <td><a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("departments_show", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "name", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("departments_show", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("departments_edit", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['department'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("departments_new");
        echo "\">Create a new department</a>
        </li>
    </ul>
";
        
        $__internal_65515f725494397dda995af5838d413b58512a0a2954d399506787095a2ded87->leave($__internal_65515f725494397dda995af5838d413b58512a0a2954d399506787095a2ded87_prof);

    }

    public function getTemplateName()
    {
        return ":departments:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 36,  91 => 31,  79 => 25,  73 => 22,  66 => 18,  60 => 17,  57 => 16,  53 => 15,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Departments list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for department in departments %}
            <tr>
                <td><a href=\"{{ path('departments_show', { 'id': department.id }) }}\">{{ department.id }}</a></td>
                <td>{{ department.name }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('departments_show', { 'id': department.id }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('departments_edit', { 'id': department.id }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('departments_new') }}\">Create a new department</a>
        </li>
    </ul>
{% endblock %}
", ":departments:index.html.twig", "/var/www/parkway/api/app/Resources/views/departments/index.html.twig");
    }
}
