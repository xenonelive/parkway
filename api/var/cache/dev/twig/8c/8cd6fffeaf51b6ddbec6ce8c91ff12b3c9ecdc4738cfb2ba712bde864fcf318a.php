<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_4effe8aa85ca45cff2be329eea32868e642facf818d6c9915011acaa6fe83de8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0592e899aeaddd2fae817bb8527b7795c6dc9a49e11da99c69771f7c2ff47fe9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0592e899aeaddd2fae817bb8527b7795c6dc9a49e11da99c69771f7c2ff47fe9->enter($__internal_0592e899aeaddd2fae817bb8527b7795c6dc9a49e11da99c69771f7c2ff47fe9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_0592e899aeaddd2fae817bb8527b7795c6dc9a49e11da99c69771f7c2ff47fe9->leave($__internal_0592e899aeaddd2fae817bb8527b7795c6dc9a49e11da99c69771f7c2ff47fe9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
