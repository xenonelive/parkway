<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_308be1fd0a9e45d43b5a162e951263a3f8b7bacf293d82899ceafcf789332a22 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_089b56dd31afda841c961e20d151283b5d371375f66d36a99a9ea9a407ce7d18 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_089b56dd31afda841c961e20d151283b5d371375f66d36a99a9ea9a407ce7d18->enter($__internal_089b56dd31afda841c961e20d151283b5d371375f66d36a99a9ea9a407ce7d18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_089b56dd31afda841c961e20d151283b5d371375f66d36a99a9ea9a407ce7d18->leave($__internal_089b56dd31afda841c961e20d151283b5d371375f66d36a99a9ea9a407ce7d18_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
