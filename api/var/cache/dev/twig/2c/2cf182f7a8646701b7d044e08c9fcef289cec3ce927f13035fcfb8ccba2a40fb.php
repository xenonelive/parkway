<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_7b8a4e6245ee79122f7d96a87777db70e5374becf7661f9b7559e045ab8a0d53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_24ec749ad222d47af3c648a872901f9a327200c1ee796ce5d06108597b295017 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24ec749ad222d47af3c648a872901f9a327200c1ee796ce5d06108597b295017->enter($__internal_24ec749ad222d47af3c648a872901f9a327200c1ee796ce5d06108597b295017_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_24ec749ad222d47af3c648a872901f9a327200c1ee796ce5d06108597b295017->leave($__internal_24ec749ad222d47af3c648a872901f9a327200c1ee796ce5d06108597b295017_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_4c7809fb66db22cb8445d154b7d0ee52e95922348e0289ff9bbf018d20b65358 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c7809fb66db22cb8445d154b7d0ee52e95922348e0289ff9bbf018d20b65358->enter($__internal_4c7809fb66db22cb8445d154b7d0ee52e95922348e0289ff9bbf018d20b65358_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_4c7809fb66db22cb8445d154b7d0ee52e95922348e0289ff9bbf018d20b65358->leave($__internal_4c7809fb66db22cb8445d154b7d0ee52e95922348e0289ff9bbf018d20b65358_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "/var/www/parkway/api/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
