<?php

/* WebProfilerBundle:Collector:exception.css.twig */
class __TwigTemplate_4b80c76f5e8381b8a3b0eeebf340fc3caa0bec562f9beae1320c05125a20a6a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b1d3261525b678ba43ccb227a49c224738e0c2d9e3690834759e982cab81258d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b1d3261525b678ba43ccb227a49c224738e0c2d9e3690834759e982cab81258d->enter($__internal_b1d3261525b678ba43ccb227a49c224738e0c2d9e3690834759e982cab81258d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        // line 1
        echo ".sf-reset .traces {
    padding: 0 0 1em 1.5em;
}
.sf-reset .traces a {
    font-size: 14px;
}
.sf-reset .traces abbr {
    border-bottom-color: #AAA;
    padding-bottom: 2px;
}
.sf-reset .traces li {
    color: #222;
    font-size: 14px;
    padding: 5px 0;
    list-style-type: decimal;
    margin: 0 0 0 1em;
}
.sf-reset .traces li.selected {
    background: rgba(255, 255, 153, 0.5);
}

.sf-reset .traces ol li {
    font-size: 12px;
    color: #777;
}
.sf-reset #logs .traces li.error {
    color: #AA3333;
}
.sf-reset #logs .traces li.warning {
    background: #FFCC00;
}
.sf-reset .trace {
    border: 1px solid #DDD;
    background: #FFF;
    padding: 10px;
    overflow: auto;
    margin: 1em 0;
}
.sf-reset .trace code,
#traces-text pre {
    font-size: 13px;
}
.sf-reset .block-exception {
    margin-bottom: 2em;
    background-color: #FFF;
    border: 1px solid #EEE;
    padding: 28px;
    word-wrap: break-word;
    overflow: hidden;
}
.sf-reset .block-exception h1 {
    font-size: 21px;
    font-weight: normal;
    margin: 0 0 12px;
}
.sf-reset .block-exception .linked {
    margin-top: 1em;
}

.sf-reset .block {
    margin-bottom: 2em;
}
.sf-reset .block h2 {
    font-size: 16px;
}
.sf-reset .block-exception div {
    font-size: 14px;
}
.sf-reset .block-exception-detected .illustration-exception,
.sf-reset .block-exception-detected .text-exception {
    float: left;
}
.sf-reset .block-exception-detected .illustration-exception {
    width: 110px;
}
.sf-reset .block-exception-detected .text-exception {
    width: 650px;
    margin-left: 20px;
    padding: 30px 44px 24px 46px;
    position: relative;
}
.sf-reset .text-exception .open-quote,
.sf-reset .text-exception .close-quote {
    position: absolute;
}
.sf-reset .open-quote {
    top: 0;
    left: 0;
}
.sf-reset .close-quote {
    bottom: 0;
    right: 50px;
}
.sf-reset .toggle {
    vertical-align: middle;
}
";
        
        $__internal_b1d3261525b678ba43ccb227a49c224738e0c2d9e3690834759e982cab81258d->leave($__internal_b1d3261525b678ba43ccb227a49c224738e0c2d9e3690834759e982cab81258d_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.css.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source(".sf-reset .traces {
    padding: 0 0 1em 1.5em;
}
.sf-reset .traces a {
    font-size: 14px;
}
.sf-reset .traces abbr {
    border-bottom-color: #AAA;
    padding-bottom: 2px;
}
.sf-reset .traces li {
    color: #222;
    font-size: 14px;
    padding: 5px 0;
    list-style-type: decimal;
    margin: 0 0 0 1em;
}
.sf-reset .traces li.selected {
    background: rgba(255, 255, 153, 0.5);
}

.sf-reset .traces ol li {
    font-size: 12px;
    color: #777;
}
.sf-reset #logs .traces li.error {
    color: #AA3333;
}
.sf-reset #logs .traces li.warning {
    background: #FFCC00;
}
.sf-reset .trace {
    border: 1px solid #DDD;
    background: #FFF;
    padding: 10px;
    overflow: auto;
    margin: 1em 0;
}
.sf-reset .trace code,
#traces-text pre {
    font-size: 13px;
}
.sf-reset .block-exception {
    margin-bottom: 2em;
    background-color: #FFF;
    border: 1px solid #EEE;
    padding: 28px;
    word-wrap: break-word;
    overflow: hidden;
}
.sf-reset .block-exception h1 {
    font-size: 21px;
    font-weight: normal;
    margin: 0 0 12px;
}
.sf-reset .block-exception .linked {
    margin-top: 1em;
}

.sf-reset .block {
    margin-bottom: 2em;
}
.sf-reset .block h2 {
    font-size: 16px;
}
.sf-reset .block-exception div {
    font-size: 14px;
}
.sf-reset .block-exception-detected .illustration-exception,
.sf-reset .block-exception-detected .text-exception {
    float: left;
}
.sf-reset .block-exception-detected .illustration-exception {
    width: 110px;
}
.sf-reset .block-exception-detected .text-exception {
    width: 650px;
    margin-left: 20px;
    padding: 30px 44px 24px 46px;
    position: relative;
}
.sf-reset .text-exception .open-quote,
.sf-reset .text-exception .close-quote {
    position: absolute;
}
.sf-reset .open-quote {
    top: 0;
    left: 0;
}
.sf-reset .close-quote {
    bottom: 0;
    right: 50px;
}
.sf-reset .toggle {
    vertical-align: middle;
}
", "WebProfilerBundle:Collector:exception.css.twig", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.css.twig");
    }
}
