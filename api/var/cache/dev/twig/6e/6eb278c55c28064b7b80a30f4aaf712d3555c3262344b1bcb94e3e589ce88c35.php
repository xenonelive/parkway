<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_4be6106ed5b99ab8cc298574b1013c99065035b5ca621bed11bb04f4eb457536 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f30c84abea2743e736878f6e71789038fa743ab24acf23a4e1d0f7119afd1df8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f30c84abea2743e736878f6e71789038fa743ab24acf23a4e1d0f7119afd1df8->enter($__internal_f30c84abea2743e736878f6e71789038fa743ab24acf23a4e1d0f7119afd1df8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_f30c84abea2743e736878f6e71789038fa743ab24acf23a4e1d0f7119afd1df8->leave($__internal_f30c84abea2743e736878f6e71789038fa743ab24acf23a4e1d0f7119afd1df8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
