<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_a5224dd6c0bf283d135b1493929c2d0ab8bd828efac00512fe92512b944814c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_55010b7144d5dc63e8837943496d5af248956208ba8a96c6c8878a550f6430b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55010b7144d5dc63e8837943496d5af248956208ba8a96c6c8878a550f6430b0->enter($__internal_55010b7144d5dc63e8837943496d5af248956208ba8a96c6c8878a550f6430b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_55010b7144d5dc63e8837943496d5af248956208ba8a96c6c8878a550f6430b0->leave($__internal_55010b7144d5dc63e8837943496d5af248956208ba8a96c6c8878a550f6430b0_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_8b952f11326fb3e17eb5eb11d3e927cd5fc4aec2af21360759418172a33a6dc0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b952f11326fb3e17eb5eb11d3e927cd5fc4aec2af21360759418172a33a6dc0->enter($__internal_8b952f11326fb3e17eb5eb11d3e927cd5fc4aec2af21360759418172a33a6dc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 4, $this->getSourceContext()); })()), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) || array_key_exists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new Twig_Error_Runtime('Variable "confirmationUrl" does not exist.', 4, $this->getSourceContext()); })())), "FOSUserBundle");
        
        $__internal_8b952f11326fb3e17eb5eb11d3e927cd5fc4aec2af21360759418172a33a6dc0->leave($__internal_8b952f11326fb3e17eb5eb11d3e927cd5fc4aec2af21360759418172a33a6dc0_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_d9cb28fe6849f9e9edc04a8426b48692cddd9ff939a0abdbe77eaaa816638f5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9cb28fe6849f9e9edc04a8426b48692cddd9ff939a0abdbe77eaaa816638f5c->enter($__internal_d9cb28fe6849f9e9edc04a8426b48692cddd9ff939a0abdbe77eaaa816638f5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 10, $this->getSourceContext()); })()), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) || array_key_exists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new Twig_Error_Runtime('Variable "confirmationUrl" does not exist.', 10, $this->getSourceContext()); })())), "FOSUserBundle");
        echo "
";
        
        $__internal_d9cb28fe6849f9e9edc04a8426b48692cddd9ff939a0abdbe77eaaa816638f5c->leave($__internal_d9cb28fe6849f9e9edc04a8426b48692cddd9ff939a0abdbe77eaaa816638f5c_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_cd6f75f1e665e9f1d67ab3ff2fcae1c8a1aa27f46b871c6157fcdcb0cebb73b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd6f75f1e665e9f1d67ab3ff2fcae1c8a1aa27f46b871c6157fcdcb0cebb73b4->enter($__internal_cd6f75f1e665e9f1d67ab3ff2fcae1c8a1aa27f46b871c6157fcdcb0cebb73b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_cd6f75f1e665e9f1d67ab3ff2fcae1c8a1aa27f46b871c6157fcdcb0cebb73b4->leave($__internal_cd6f75f1e665e9f1d67ab3ff2fcae1c8a1aa27f46b871c6157fcdcb0cebb73b4_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 10,  52 => 8,  45 => 4,  39 => 2,  32 => 13,  30 => 8,  27 => 7,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "/var/www/parkway/api/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/email.txt.twig");
    }
}
