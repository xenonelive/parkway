<?php

/* :email:password_resetting.email.twig */
class __TwigTemplate_c7daae1c536b26ca79ea8a7b1caac2faa91fe695b76e997f92bf20f38da45052 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0e922540e97fc142213a8485f78e0e513cbfc54d47d8bd72febc3e827c22f45 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0e922540e97fc142213a8485f78e0e513cbfc54d47d8bd72febc3e827c22f45->enter($__internal_e0e922540e97fc142213a8485f78e0e513cbfc54d47d8bd72febc3e827c22f45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":email:password_resetting.email.twig"));

        // line 1
        $this->displayBlock('subject', $context, $blocks);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('body_text', $context, $blocks);
        
        $__internal_e0e922540e97fc142213a8485f78e0e513cbfc54d47d8bd72febc3e827c22f45->leave($__internal_e0e922540e97fc142213a8485f78e0e513cbfc54d47d8bd72febc3e827c22f45_prof);

    }

    // line 1
    public function block_subject($context, array $blocks = array())
    {
        $__internal_b9b6c7028586900e20cb1d0d307fe91ac4ad5a47bb3bca3d850fc06646da291d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b9b6c7028586900e20cb1d0d307fe91ac4ad5a47bb3bca3d850fc06646da291d->enter($__internal_b9b6c7028586900e20cb1d0d307fe91ac4ad5a47bb3bca3d850fc06646da291d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        echo "Resetting your password";
        
        $__internal_b9b6c7028586900e20cb1d0d307fe91ac4ad5a47bb3bca3d850fc06646da291d->leave($__internal_b9b6c7028586900e20cb1d0d307fe91ac4ad5a47bb3bca3d850fc06646da291d_prof);

    }

    // line 3
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_eec26f2de71ed6b868f13121c77a6d0cbda2aaafbae1a37fda2f6df3f4f0e569 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eec26f2de71ed6b868f13121c77a6d0cbda2aaafbae1a37fda2f6df3f4f0e569->enter($__internal_eec26f2de71ed6b868f13121c77a6d0cbda2aaafbae1a37fda2f6df3f4f0e569_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 4
        echo "    ";
        // line 5
        echo "        Hello ";
        echo twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 5, $this->getSourceContext()); })()), "username", array());
        echo " !

        You can reset your password by accessing ";
        // line 7
        echo (isset($context["confirmationUrl"]) || array_key_exists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new Twig_Error_Runtime('Variable "confirmationUrl" does not exist.', 7, $this->getSourceContext()); })());
        echo "

        Greetings,
        the App team
    ";
        
        $__internal_eec26f2de71ed6b868f13121c77a6d0cbda2aaafbae1a37fda2f6df3f4f0e569->leave($__internal_eec26f2de71ed6b868f13121c77a6d0cbda2aaafbae1a37fda2f6df3f4f0e569_prof);

    }

    public function getTemplateName()
    {
        return ":email:password_resetting.email.twig";
    }

    public function getDebugInfo()
    {
        return array (  62 => 7,  56 => 5,  54 => 4,  48 => 3,  36 => 1,  29 => 3,  26 => 2,  24 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block subject %}Resetting your password{% endblock %}

{% block body_text %}
    {% autoescape false %}
        Hello {{ user.username }} !

        You can reset your password by accessing {{ confirmationUrl }}

        Greetings,
        the App team
    {% endautoescape %}
{% endblock %}", ":email:password_resetting.email.twig", "/var/www/parkway/api/app/Resources/views/email/password_resetting.email.twig");
    }
}
