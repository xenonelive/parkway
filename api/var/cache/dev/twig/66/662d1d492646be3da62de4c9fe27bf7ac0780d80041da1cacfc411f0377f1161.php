<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_b323315ba9015c5a2a23a8f1b17d095d412a55eebf84b0678e50725230e2cfee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_60b27ef5b1706cf1a36eb42e3fbd8a80e4935512ae96afa0d0b5c7571c5a8985 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60b27ef5b1706cf1a36eb42e3fbd8a80e4935512ae96afa0d0b5c7571c5a8985->enter($__internal_60b27ef5b1706cf1a36eb42e3fbd8a80e4935512ae96afa0d0b5c7571c5a8985_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_60b27ef5b1706cf1a36eb42e3fbd8a80e4935512ae96afa0d0b5c7571c5a8985->leave($__internal_60b27ef5b1706cf1a36eb42e3fbd8a80e4935512ae96afa0d0b5c7571c5a8985_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_958b3ce907390162bd92f6274f24e770ceb98d2df4ecc3c7c850f7fb34701021 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_958b3ce907390162bd92f6274f24e770ceb98d2df4ecc3c7c850f7fb34701021->enter($__internal_958b3ce907390162bd92f6274f24e770ceb98d2df4ecc3c7c850f7fb34701021_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 4, $this->getSourceContext()); })()), "username", array())), "FOSUserBundle");
        
        $__internal_958b3ce907390162bd92f6274f24e770ceb98d2df4ecc3c7c850f7fb34701021->leave($__internal_958b3ce907390162bd92f6274f24e770ceb98d2df4ecc3c7c850f7fb34701021_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_2a540e081af16bfe498120b92f0656fdca57ff5fa9ddc74f9c30292184942efc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a540e081af16bfe498120b92f0656fdca57ff5fa9ddc74f9c30292184942efc->enter($__internal_2a540e081af16bfe498120b92f0656fdca57ff5fa9ddc74f9c30292184942efc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 10, $this->getSourceContext()); })()), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) || array_key_exists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new Twig_Error_Runtime('Variable "confirmationUrl" does not exist.', 10, $this->getSourceContext()); })())), "FOSUserBundle");
        echo "
";
        
        $__internal_2a540e081af16bfe498120b92f0656fdca57ff5fa9ddc74f9c30292184942efc->leave($__internal_2a540e081af16bfe498120b92f0656fdca57ff5fa9ddc74f9c30292184942efc_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_01f84fbc1576c74f25447515d548967a1cda66b63e7034564a5e9a684a948df5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01f84fbc1576c74f25447515d548967a1cda66b63e7034564a5e9a684a948df5->enter($__internal_01f84fbc1576c74f25447515d548967a1cda66b63e7034564a5e9a684a948df5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_01f84fbc1576c74f25447515d548967a1cda66b63e7034564a5e9a684a948df5->leave($__internal_01f84fbc1576c74f25447515d548967a1cda66b63e7034564a5e9a684a948df5_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 10,  52 => 8,  45 => 4,  39 => 2,  32 => 13,  30 => 8,  27 => 7,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "/var/www/parkway/api/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}
