<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_5c67e18b39bdafb235eb761952a9953e7649c48c2405c82081b3d603cb8cb2b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3c730bfac4f0838323f393736e416839cdae9d41b50285e8c7299f899094d7b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b3c730bfac4f0838323f393736e416839cdae9d41b50285e8c7299f899094d7b->enter($__internal_b3c730bfac4f0838323f393736e416839cdae9d41b50285e8c7299f899094d7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b3c730bfac4f0838323f393736e416839cdae9d41b50285e8c7299f899094d7b->leave($__internal_b3c730bfac4f0838323f393736e416839cdae9d41b50285e8c7299f899094d7b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5e029023dcb320e2f72b563b74fc4697440a0673dcbf73537719bb51f312d0fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e029023dcb320e2f72b563b74fc4697440a0673dcbf73537719bb51f312d0fb->enter($__internal_5e029023dcb320e2f72b563b74fc4697440a0673dcbf73537719bb51f312d0fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_5e029023dcb320e2f72b563b74fc4697440a0673dcbf73537719bb51f312d0fb->leave($__internal_5e029023dcb320e2f72b563b74fc4697440a0673dcbf73537719bb51f312d0fb_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:show.html.twig", "/var/www/parkway/api/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show.html.twig");
    }
}
