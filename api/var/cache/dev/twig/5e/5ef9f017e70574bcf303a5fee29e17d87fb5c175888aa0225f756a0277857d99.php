<?php

/* EDUBundle:users:layout_new_user.html.twig */
class __TwigTemplate_0bcc8a5124f3657a6b71de38fce6ce249304920e5df662a9074753536b9674bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EDUBundle::layout.html.twig", "EDUBundle:users:layout_new_user.html.twig", 1);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EDUBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c994f650a740d3d11e83d362c4f2b126e73e167f3b3b77f87cbf4e4bcb3772f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c994f650a740d3d11e83d362c4f2b126e73e167f3b3b77f87cbf4e4bcb3772f6->enter($__internal_c994f650a740d3d11e83d362c4f2b126e73e167f3b3b77f87cbf4e4bcb3772f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:users:layout_new_user.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c994f650a740d3d11e83d362c4f2b126e73e167f3b3b77f87cbf4e4bcb3772f6->leave($__internal_c994f650a740d3d11e83d362c4f2b126e73e167f3b3b77f87cbf4e4bcb3772f6_prof);

    }

    // line 3
    public function block_cmi_body($context, array $blocks = array())
    {
        $__internal_8fde67220a070464ffd334efa26c288516a27eba9a8ffe034ff0a5c2929498d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8fde67220a070464ffd334efa26c288516a27eba9a8ffe034ff0a5c2929498d4->enter($__internal_8fde67220a070464ffd334efa26c288516a27eba9a8ffe034ff0a5c2929498d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "cmi_body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-md-2\">
            
        </div>
        <div class=\"col-md-6\">
            ";
        // line 9
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext()); })()), 'form_start');
        echo "
            ";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 10, $this->getSourceContext()); })()), 'widget');
        echo "
            ";
        // line 11
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 11, $this->getSourceContext()); })()), 'form_end');
        echo "
        </div>
    </div>
";
        
        $__internal_8fde67220a070464ffd334efa26c288516a27eba9a8ffe034ff0a5c2929498d4->leave($__internal_8fde67220a070464ffd334efa26c288516a27eba9a8ffe034ff0a5c2929498d4_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:users:layout_new_user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 11,  51 => 10,  47 => 9,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EDUBundle::layout.html.twig\" %}
    
{% block cmi_body %}
    <div class=\"row\">
        <div class=\"col-md-2\">
            
        </div>
        <div class=\"col-md-6\">
            {{form_start(form)}}
            {{form_widget(form)}}
            {{form_end(form)}}
        </div>
    </div>
{% endblock %}", "EDUBundle:users:layout_new_user.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/users/layout_new_user.html.twig");
    }
}
