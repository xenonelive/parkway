<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_84ac57a2d0aae3bfcc4c4b17a69d3b90d76a0001ec426157e62c1ca08ed63534 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ef69183bb0524d26a1208d2691c84ccc4c28f1fc31c4575aa51233ecddae7b31 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef69183bb0524d26a1208d2691c84ccc4c28f1fc31c4575aa51233ecddae7b31->enter($__internal_ef69183bb0524d26a1208d2691c84ccc4c28f1fc31c4575aa51233ecddae7b31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ef69183bb0524d26a1208d2691c84ccc4c28f1fc31c4575aa51233ecddae7b31->leave($__internal_ef69183bb0524d26a1208d2691c84ccc4c28f1fc31c4575aa51233ecddae7b31_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_aa8cd941e543201df88b219e741514837519de05c1a22bed99a19f88f4d8ec8f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa8cd941e543201df88b219e741514837519de05c1a22bed99a19f88f4d8ec8f->enter($__internal_aa8cd941e543201df88b219e741514837519de05c1a22bed99a19f88f4d8ec8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_aa8cd941e543201df88b219e741514837519de05c1a22bed99a19f88f4d8ec8f->leave($__internal_aa8cd941e543201df88b219e741514837519de05c1a22bed99a19f88f4d8ec8f_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "/var/www/parkway/api/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}
