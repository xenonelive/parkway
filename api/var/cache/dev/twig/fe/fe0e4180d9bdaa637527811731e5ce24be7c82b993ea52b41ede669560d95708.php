<?php

/* EDUBundle:departments:layout_all_departments.html.twig */
class __TwigTemplate_cc41dfc2de7a78760ec60410e978692abe102451393b8c6f7140474b6211cc9b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EDUBundle::layout.html.twig", "EDUBundle:departments:layout_all_departments.html.twig", 1);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EDUBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2e9173c1d90d11fe9d2b331272a7fe79cd003566358dfb3b81bbba1e04598dc6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2e9173c1d90d11fe9d2b331272a7fe79cd003566358dfb3b81bbba1e04598dc6->enter($__internal_2e9173c1d90d11fe9d2b331272a7fe79cd003566358dfb3b81bbba1e04598dc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:departments:layout_all_departments.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2e9173c1d90d11fe9d2b331272a7fe79cd003566358dfb3b81bbba1e04598dc6->leave($__internal_2e9173c1d90d11fe9d2b331272a7fe79cd003566358dfb3b81bbba1e04598dc6_prof);

    }

    // line 3
    public function block_cmi_body($context, array $blocks = array())
    {
        $__internal_d0f25d810be9372544331993d9031f06c8c296222a4eb869d652b11763590189 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d0f25d810be9372544331993d9031f06c8c296222a4eb869d652b11763590189->enter($__internal_d0f25d810be9372544331993d9031f06c8c296222a4eb869d652b11763590189_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "cmi_body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 6, $this->getSourceContext()); })()), "html", null, true);
        echo "/departments/new\" class=\"btn btn-success
btn-xs\">";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Add New Department"), "html", null, true);
        echo "</a>
            <br /><br />
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Id"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Name"), "html", null, true);
        echo "</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Actions"), "html", null, true);
        echo "</th>
                            </tr>
                        </thead>
                        <tbody>
                        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["departments"]) || array_key_exists("departments", $context) ? $context["departments"] : (function () { throw new Twig_Error_Runtime('Variable "departments" does not exist.', 20, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["department"]) {
            // line 21
            echo "                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            ";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()), "html", null, true);
            echo "
                                        </td>
                                        <td>
                                            ";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "name", array()), "html", null, true);
            echo "
                                        </td>
                                        <td>
                                            <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 29, $this->getSourceContext()); })()), "html", null, true);
            echo "/departments/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()), "html", null, true);
            echo "\" class=\"btn btn-xs btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("View"), "html", null, true);
            echo "</a>
                                            <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 30, $this->getSourceContext()); })()), "html", null, true);
            echo "/departments/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()), "html", null, true);
            echo "/edit\" class=\"btn btn-xs btn-default\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit"), "html", null, true);
            echo "</a>
                                            <a href=\"";
            // line 31
            echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new Twig_Error_Runtime('Variable "base_url" does not exist.', 31, $this->getSourceContext()); })()), "html", null, true);
            // line 32
            echo "/departments/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["department"], "id", array()), "html", null, true);
            echo "\" class=\"btn btn-xs btn-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Delete"), "html", null, true);
            echo "</a>

                                        </td>
                                    </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['department'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
";
        
        $__internal_d0f25d810be9372544331993d9031f06c8c296222a4eb869d652b11763590189->leave($__internal_d0f25d810be9372544331993d9031f06c8c296222a4eb869d652b11763590189_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:departments:layout_all_departments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 37,  111 => 32,  109 => 31,  101 => 30,  93 => 29,  87 => 26,  81 => 23,  77 => 21,  73 => 20,  66 => 16,  62 => 15,  58 => 14,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EDUBundle::layout.html.twig\" %}

{% block cmi_body %}
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <a href=\"{{ base_url}}/departments/new\" class=\"btn btn-success
btn-xs\">{{ 'Add New Department'|trans }}</a>
            <br /><br />
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <table width=\"100%\" class=\"table table-striped table-bordered table-hover dataTable no-footer dtr-inline\" id=\"dataTables-example\" role=\"grid\" aria-describedby=\"dataTables-example_info\" style=\"width: 100%;\">
                        <thead>
                            <tr role=\"row\">
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Id'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Name'|trans }}</th>
                                <th  tabindex=\"0\"  rowspan=\"1\" colspan=\"1\" >{{ 'Actions'|trans }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        {% for department in departments %}
                                    <tr class=\"gradeA odd\" role=\"row\">
                                        <td>
                                            {{ department.id }}
                                        </td>
                                        <td>
                                            {{ department.name }}
                                        </td>
                                        <td>
                                            <a href=\"{{ base_url }}/departments/{{ department.id }}\" class=\"btn btn-xs btn-primary\">{{ 'View'|trans }}</a>
                                            <a href=\"{{ base_url }}/departments/{{ department.id }}/edit\" class=\"btn btn-xs btn-default\">{{ 'Edit'|trans }}</a>
                                            <a href=\"{{ base_url
}}/departments/{{ department.id }}\" class=\"btn btn-xs btn-danger\">{{ 'Delete'|trans }}</a>

                                        </td>
                                    </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
{% endblock %}
", "EDUBundle:departments:layout_all_departments.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/departments/layout_all_departments.html.twig");
    }
}
