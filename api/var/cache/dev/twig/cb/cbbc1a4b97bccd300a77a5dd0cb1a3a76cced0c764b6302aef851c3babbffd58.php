<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_fbeb57ca8c4330b19e437f7db0bc50c8d6dedbf4c7162169a7ea8495133bf930 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0fd4597c23d7c0f5625837cb9b0258b3b1411b490e5e5096457da807ab856157 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0fd4597c23d7c0f5625837cb9b0258b3b1411b490e5e5096457da807ab856157->enter($__internal_0fd4597c23d7c0f5625837cb9b0258b3b1411b490e5e5096457da807ab856157_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0fd4597c23d7c0f5625837cb9b0258b3b1411b490e5e5096457da807ab856157->leave($__internal_0fd4597c23d7c0f5625837cb9b0258b3b1411b490e5e5096457da807ab856157_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_28dc28c153af3e48e260ac8ae17f5a264d08d18c0e48eafeb02a24012a02a67b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28dc28c153af3e48e260ac8ae17f5a264d08d18c0e48eafeb02a24012a02a67b->enter($__internal_28dc28c153af3e48e260ac8ae17f5a264d08d18c0e48eafeb02a24012a02a67b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_28dc28c153af3e48e260ac8ae17f5a264d08d18c0e48eafeb02a24012a02a67b->leave($__internal_28dc28c153af3e48e260ac8ae17f5a264d08d18c0e48eafeb02a24012a02a67b_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_1cef0c7fe5c77e0488165964ee21f77f5a6f073241afc098dd095b2035909606 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1cef0c7fe5c77e0488165964ee21f77f5a6f073241afc098dd095b2035909606->enter($__internal_1cef0c7fe5c77e0488165964ee21f77f5a6f073241afc098dd095b2035909606_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) || array_key_exists("location", $context) ? $context["location"] : (function () { throw new Twig_Error_Runtime('Variable "location" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) || array_key_exists("location", $context) ? $context["location"] : (function () { throw new Twig_Error_Runtime('Variable "location" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_1cef0c7fe5c77e0488165964ee21f77f5a6f073241afc098dd095b2035909606->leave($__internal_1cef0c7fe5c77e0488165964ee21f77f5a6f073241afc098dd095b2035909606_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
