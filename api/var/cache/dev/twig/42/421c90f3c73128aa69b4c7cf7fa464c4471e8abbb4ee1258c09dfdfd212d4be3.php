<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_9d14b81f0bc64d1bbb4b6622e2e0d5ba1fa9f4e8d7d7fac390a75206b4bfb4af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9d2c030da68ecc3802973dd446418cfcd1d4449f7c298a2c434fcc89bff8e79d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d2c030da68ecc3802973dd446418cfcd1d4449f7c298a2c434fcc89bff8e79d->enter($__internal_9d2c030da68ecc3802973dd446418cfcd1d4449f7c298a2c434fcc89bff8e79d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_9d2c030da68ecc3802973dd446418cfcd1d4449f7c298a2c434fcc89bff8e79d->leave($__internal_9d2c030da68ecc3802973dd446418cfcd1d4449f7c298a2c434fcc89bff8e79d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
