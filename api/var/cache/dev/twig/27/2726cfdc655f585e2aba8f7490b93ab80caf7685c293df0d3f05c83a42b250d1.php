<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_5fa4f4d9ef14614a324aef25a309bb039ca0b18a7be282bde79eb35af0450c3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9e3f904a6285bceb9b86de25f8df9c8ee2c0b52af07b692e46e5decc7833a4af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e3f904a6285bceb9b86de25f8df9c8ee2c0b52af07b692e46e5decc7833a4af->enter($__internal_9e3f904a6285bceb9b86de25f8df9c8ee2c0b52af07b692e46e5decc7833a4af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9e3f904a6285bceb9b86de25f8df9c8ee2c0b52af07b692e46e5decc7833a4af->leave($__internal_9e3f904a6285bceb9b86de25f8df9c8ee2c0b52af07b692e46e5decc7833a4af_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_8911376ea65afbefcd54d87131e90ad421b03761e98d910edc91e89cb322a8c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8911376ea65afbefcd54d87131e90ad421b03761e98d910edc91e89cb322a8c6->enter($__internal_8911376ea65afbefcd54d87131e90ad421b03761e98d910edc91e89cb322a8c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_8911376ea65afbefcd54d87131e90ad421b03761e98d910edc91e89cb322a8c6->leave($__internal_8911376ea65afbefcd54d87131e90ad421b03761e98d910edc91e89cb322a8c6_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_9d99fbb99171b17304d6111ea76dd6743f6475bccaa2737142f2a0276c1e3736 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d99fbb99171b17304d6111ea76dd6743f6475bccaa2737142f2a0276c1e3736->enter($__internal_9d99fbb99171b17304d6111ea76dd6743f6475bccaa2737142f2a0276c1e3736_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 8, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo ")
";
        
        $__internal_9d99fbb99171b17304d6111ea76dd6743f6475bccaa2737142f2a0276c1e3736->leave($__internal_9d99fbb99171b17304d6111ea76dd6743f6475bccaa2737142f2a0276c1e3736_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_085dac8f836dbd3d662dc45ea616e3c64cb2834e72faa649b2b251c9e5746f56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_085dac8f836dbd3d662dc45ea616e3c64cb2834e72faa649b2b251c9e5746f56->enter($__internal_085dac8f836dbd3d662dc45ea616e3c64cb2834e72faa649b2b251c9e5746f56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_085dac8f836dbd3d662dc45ea616e3c64cb2834e72faa649b2b251c9e5746f56->leave($__internal_085dac8f836dbd3d662dc45ea616e3c64cb2834e72faa649b2b251c9e5746f56_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "/var/www/parkway/api/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
