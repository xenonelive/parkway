<?php

/* EDUBundle:Default/form:createMessage.html.twig */
class __TwigTemplate_d348a53587bc078de28dfcee2523626a73a4e7411a94596f12556b591b641a59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("CMIBundle::layout.html.twig", "EDUBundle:Default/form:createMessage.html.twig", 3);
        $this->blocks = array(
            'cmi_body' => array($this, 'block_cmi_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CMIBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7eb73329def1780d8c69e2a9e08440aa7ae7c226340a6d08b046fb75ed0935f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7eb73329def1780d8c69e2a9e08440aa7ae7c226340a6d08b046fb75ed0935f->enter($__internal_d7eb73329def1780d8c69e2a9e08440aa7ae7c226340a6d08b046fb75ed0935f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EDUBundle:Default/form:createMessage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d7eb73329def1780d8c69e2a9e08440aa7ae7c226340a6d08b046fb75ed0935f->leave($__internal_d7eb73329def1780d8c69e2a9e08440aa7ae7c226340a6d08b046fb75ed0935f_prof);

    }

    // line 5
    public function block_cmi_body($context, array $blocks = array())
    {
        $__internal_5ca0eaffd8c2089b2d46ba84018dd20093198c8c67463fb0acbbc51858146a30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ca0eaffd8c2089b2d46ba84018dd20093198c8c67463fb0acbbc51858146a30->enter($__internal_5ca0eaffd8c2089b2d46ba84018dd20093198c8c67463fb0acbbc51858146a30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "cmi_body"));

        // line 6
        echo "
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    ";
        // line 11
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 11, $this->getSourceContext()); })()), 'form_start', array("attr" => array("role" => "form", "enctype" => "multipart/form-data")));
        echo "
                    ";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 12, $this->getSourceContext()); })()), 'widget');
        echo "
                    <div class=\"panel panel-default\">
                        <div class=\"panel-body\">
                            <div class=\"form-group\">
                                <label>File</label>
                                <input type=\"file\" name=\"image\" />
                            </div>
                        </div>
                    </div>
                    <button type=\"submit\">Save</button>
                    ";
        // line 22
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 22, $this->getSourceContext()); })()), 'form_end');
        echo "

                </div>
            </div>
        </div>
    </div>

";
        
        $__internal_5ca0eaffd8c2089b2d46ba84018dd20093198c8c67463fb0acbbc51858146a30->leave($__internal_5ca0eaffd8c2089b2d46ba84018dd20093198c8c67463fb0acbbc51858146a30_prof);

    }

    public function getTemplateName()
    {
        return "EDUBundle:Default/form:createMessage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 22,  51 => 12,  47 => 11,  40 => 6,  34 => 5,  11 => 3,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("

{% extends \"CMIBundle::layout.html.twig\" %}

{% block cmi_body %}

    <div class=\"row\">
        <div class=\"col-lg-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    {{ form_start(form, {'attr': {'role': 'form', 'enctype': 'multipart/form-data'}}) }}
                    {{ form_widget(form) }}
                    <div class=\"panel panel-default\">
                        <div class=\"panel-body\">
                            <div class=\"form-group\">
                                <label>File</label>
                                <input type=\"file\" name=\"image\" />
                            </div>
                        </div>
                    </div>
                    <button type=\"submit\">Save</button>
                    {{ form_end(form) }}

                </div>
            </div>
        </div>
    </div>

{% endblock %}", "EDUBundle:Default/form:createMessage.html.twig", "/var/www/parkway/api/src/EDUBundle/Resources/views/Default/form/createMessage.html.twig");
    }
}
