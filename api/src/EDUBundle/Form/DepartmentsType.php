<?php

namespace EDUBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


// form input types
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 
use Symfony\Component\Form\Extension\Core\Type\ChoiceType; 
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class DepartmentsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',
					  TextType::class,
					  array('attr'  =>  array('class' => 'form-control',
											  'style' => 'margin:5px 0;'),'label' => 'Department Name'

							) );
        
        $builder->add('parentid',
						NULL,
						array(	'expanded'  => false,
								'multiple'  => false,
								'attr'  	=> array(	'class' => 'form-control',
                                'style' => 'height:auto'),
                                'placeholder' => 'none',
                                'label' =>'Select Parent Department'));

        
		$builder->add('Add User',
					  SubmitType::class,
					  array('attr'  =>  array('class' => 'btn btn-primary',
											  'style' => 'margin:15px 0;'),
							'label' =>'Submit') );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EDUBundle\Entity\Departments'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'edubundle_departments';
    }


}
