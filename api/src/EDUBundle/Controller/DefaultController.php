<?php

namespace EDUBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
#use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RequestContext;
//use DBP\InterfazIcedbpBundle\Form\DataTransformer\StringToArrayTransformer;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\DateTime;

//form
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 
use Symfony\Component\Form\Extension\Core\Type\ChoiceType; 
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
//use Symfony\Component\Form\Extension\Core\Type\NewMessageType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Loader\XliffFileLoader;

//use EDUBundle\Form as Form;
use EDUBundle\Form\UserType;
use EDUBundle\Form\UserEditType;
use EDUBundle\Form\ClassType;
use EDUBundle\Form\AddClassType;
use EDUBundle\Form\StudentEditType; 
use EDUBundle\Form\ClassSubjectType; 
use EDUBundle\Form\ClassSubjectEditType; 
use EDUBundle\Form\ClassMessageType;


//DB Entity
use EDUBundle\Entity\Users;
use EDUBundle\Entity\contact;
use EDUBundle\Entity\Audit;

// Custom libraries
use ForceUTF8\Encoding;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
		error_log('CAM',0);
        return $this->render('EDUBundle:Default:index.html.twig');
    }


    /**
     * Lists all department entities.
     *
     * @Route("/audit/all", name="audit_index")
     */
    public function auditAction()
    {
        $em = $this->getDoctrine()->getManager();

        $logs = $em->getRepository('EDUBundle:Audit')->findAll();

        return $this->render('EDUBundle:departments:layout_all_audit.html.twig', array(
            'logs' => $logs,
        ));
    }
	
	
	/*new user with address
	 *@Route("/new/user", name="new_user")
	 */
	public function newuserAction(Request $request)
	{
		$user 	 = new Users;
		$address = new contact;
		
		$form = $this->createForm(UserType::class, $user);
		
		$form->handleRequest($request);
		
		return $this->render('EDUBundle:Form:newuser.html.twig',
							 array(
									'title' => 'Add New User',
									'form' 	=> $form->createView()
        ));
	}
	
	
    /**
     * Get Users list
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route(name="user_list")
     */
    public function getUsersAction(Request $request) {
        //$em = $this->getDoctrine()->getManager();
        //$users = $em->getRepository('EDUBundle:Users')->findAll();
        //
        //$translator = $this->get('translator');
        //
        //$locale = $request->getLocale();
        //
        //return $this->render(
        //    'EDUBundle:users:layout_all_users.html.twig',
        //    array(
        //        'users'	=> $users,
        //        'title' => $translator->trans('%USERS%'),
        //        'tables' => 1,
        //        'locale' => $locale
        //    )
        //);
		//error_log('U-CAM',0);	
		if( $this->get('security.authorization_checker')->isGranted('ROLE_EDUCATOR') ||
			$this->get('security.authorization_checker')->isGranted('ROLE_TUTOR'))
		{
			return $this->redirectToRoute('message_inbox');
		}
		else
		{
			$em = $this->getDoctrine()->getManager();
			$users = $em->getRepository('EDUBundle:Users')->findAll();
			
			//translations
			$translator = $this->get('translator');
			
			$locale = $request->getLocale();
			
			return $this->render(
				'EDUBundle:users:layout_all_users.html.twig',
				array(	'users'		=> $users,
						'title' 	=> $translator->trans('%USERS%'),
						'tables' 	=> 1,
						'locale' 	=> $locale
					)
			);
		}
    }
	
    
    /**
     *Shows User information
     *
     *@Route("/user/info/{id}", name="user_info")
     */
    public function getUserInfoAction( $id, Request $request )
    {
		//translations
		
        $translator = $this->get('translator');
		
        $em     = $this->getDoctrine()->getManager();
        $user   = $em->getRepository('EDUBundle:Users')->find($id);
        
		//set updated to 0
		if( $user->getUpdated() == 1 )
		{
			$user->setUpdated(0);
			$em->persist($user);
			$em->flush();
		}
		
        return $this->render(	'EDUBundle:users:layout_user_info.html.twig',
								array(	'user' => $user,
										'title' => $translator->trans('%USER%'),
										'tables' => 1 )
        );
        
    }
	
	/**
     *Shows Teacher information
     *
     *@Route("/teacher/info/{id}", name="teacher_info")
     */
    public function getTeacherInfoAction( $id, Request $request )
    {
		//translations
        $translator = $this->get('translator');
		
        $em     = $this->getDoctrine()->getManager();
        $user   = $em->getRepository('EDUBundle:Users')->find($id);
		
		
		/*
        return $this->render(	'EDUBundle:users:layout_teacher_info.html.twig',
								array(	'user' => $user,
										'title' => $translator->trans('%USER%'),
										'tables' => 1 )
        );*/
		
		//view_info.html.twig
		
		
		
        return $this->render('EDUBundle:users:view_info.html.twig',
								array(	'user' => $user,
										'title' => $translator->trans('%USER%'),
										'tables' => 1 )
        );
		
		
		
	
    }
	
	
	/**
     *Shows Teacher information
     *
     *@Route("/student/info/{id}", name="student_info")
     */
    public function getStudentInfoAction( $id, Request $request )
    {
		//translations
        $translator = $this->get('translator');
		
        $em     = $this->getDoctrine()->getManager();
        $user   = $em->getRepository('EDUBundle:Users')->find($id);
        
        return $this->render(	'EDUBundle:users:layout_student_info.html.twig',
								array(	'user' => $user,
										'title' => $translator->trans('%USER%'),
										'tables' => 1 )
        );
    }
	
	
	
	/**
	 *	@return	all Rolese as an array
	 */
	function getRole()
    {
		$translator = $this->get('translator');
		$roles 		= array(	"ADMIN"		=>	$translator->trans("ROLE_ADMIN"),
								"EMPLOYEE"	=>	$translator->trans("EMPLOYEE"),
								);
			
		return $roles;
	}
    /**
     *Shows User information
     *
     *@Route("/user/add", name="user_new")
     */
    public function addNewUserAction(Request $request)
    {
        if($request->query->get('new_user')){
            //print_r($request->query->get('new_user'));die('HARD');
        }
		//translations
        $translator = $this->get('translator');
        $user 	 = new Users;
		$form = $this->createForm(UserType::class, $user, array('UserRole' =>$this->getRole()));
		$form->handleRequest($request);
		
		if( $form->isSubmitted() && $form->isValid() )
		{
            $pass   = $form['password']->getData();
			$role   = $form['roles']->getData();
			$role_array=$this->getRole();
			
			$keys = array_keys($role_array);
			
			// new user cannot have previously added email, phone number or contact number
			$user_email = $form['email']->getData(); // new users email
			$user_name	= $form['username']->getData(); // new users username
			$user_pnum	= $form['phonenumber']->getData(); // new users phonenumber
			$user_snum	= $form['contact']['contactNumber']->getData(); // new users phonenumber
			$userErMsg	= array(); // list of error messages as an array
			$em 	= $this->getDoctrine()->getManager();
			$users  = $em->getRepository('EDUBundle:Users')->findAll();
			
			foreach( $users as $new_user )
			{
				$exist_mail 	= $new_user->getEmail();
				$exist_uname	= $new_user->getUsername();
				$exist_phone	= $new_user->getPhoneNumber();
				$exist_contact	= $new_user->getContact()->getContactNumber();
				
				if( $exist_mail == $user_email )
				{
					$userErMsg[] = $translator->trans('%existEmail% is already in the database. Please try with new Email', array( '%existEmail%' => $exist_mail ));
				}
				
				if( $exist_uname == $user_name )
				{
					$userErMsg[] = $translator->trans('%existUname% is already in the database. Please try with new Email', array( '%existUname%' => $exist_uname ));
				}
				
				if( $exist_phone == $user_pnum )
				{
					$userErMsg[] = $translator->trans('%existPhone% is already in the database. Please try with new Phone Number', array( '%existPhone%' => $exist_phone ));
				}
				
				if( $exist_contact == $user_snum )
				{
					$userErMsg[] = $translator->trans('%existContact% is already in the database. Please try with new Phone Number', array( '%existContact%' => $exist_contact ));
				}
			}
            
            $notvalid = false;

			if( $userErMsg ) // NewUser post data has email, phone number or contact number duplicates with DB
			{
				foreach( $userErMsg as $message )
				{
					$this->addFlash('warning', $message );
                }
                $notvalid = true;
				//break;
				//return $this->redirectToRoute('user_new',array('new_user'=>1));
			}
			
			if( in_array($role_array[$keys[1]], $role) && sizeof($role) != 1 ) // role student selected with other roles
			{
				$this->addFlash('warning',
								$translator->trans('Role Emplyee cannot have Admin Role')
                            );
                $notvalid = true;
                //break;
				//return $this->redirectToRoute('user_new',array('new_user'=>1));
				//return $this->redirectToRoute('user_new');
			}
			
			if( in_array($role_array[$keys[0]], $role) && sizeof($role) != 1 ) // role student selected with other roles
			{
				$this->addFlash('warning',
								$translator->trans('Role Admin cannot have Emplyee role')
                            );
                $notvalid = true;
                //break;
				//return $this->redirectToRoute('user_new',array('new_user'=>1));
				//return $this->redirectToRoute('user_new');
            }

            if(!$notvalid){

                $user->setPlainPassword($pass);
                $user->setRoles($role);
                $user->setEnabled('1');


                // Audit Logging //
                $audit = new Audit();
                $audit->setAction('ADD USER');
                $audit->setTargetuser($user);
                $audit->setDatetime(date_create(date('Y-m-d H:i:s')));
                $audit->setUser($this->getCurrentUser());

                $em 	= $this->getDoctrine()->getManager();
                $em->persist($audit);
                $em->persist($user);
                $em->flush();

                $this->addFlash('notice',
                    $translator->trans('%USER_ADDED%')
                );

                return $this->redirectToRoute('user_list');
            }
		}

        /*
        if($request->query->get('new_user')){
            return $this->render('EDUBundle:Form:newuser.html.twig',
                array(	
                    'title' => $translator->trans('%AD_NW_SR%'),
                    'email'	=> $request->query->get('new_user'),
                    'form' 	=> $form->createView()
                ));
        }
         */
		return $this->render('EDUBundle:Form:newuser.html.twig',
            array(	
                'title' => $translator->trans('%AD_NW_SR%'),
                             'form' 	=> $form->createView()
        ));
	}
	
	/**
    *	Edit user page
    *
    *@Route("user/edit/{id}", name="user_edit_a")
    *	@return	user edit form
    */
	public function userEditAction($id, Request $request )
	{
		//translations
        $translator = $this->get('translator');
		
		$em 	= $this->getDoctrine()->getManager();
		$user   = $em->getRepository('EDUBundle:Users')->find($id);
		if (!$user)
		{
			throw $this->createNotFoundException($translator->trans('%NO_US_ID%').$id);
		}
		$form = $this->createForm(UserEditType::class, $user,
								  array('UserRole' =>$this->getRole(),
										));
		$form->handleRequest($request);
		
		
		if( $form->isSubmitted() && $form->isValid() )
		{
			$role   = $form['roles']->getData();
			$role_array=$this->getRole();
			$keys = array_keys($role_array);
				
			if( in_array($role_array[$keys[1]], $role) && sizeof($role) != 1 ) // role student selected with other roles
			{
				$this->addFlash('warning',
								$translator->trans('Role Employee cannot have Admin role.')
				);
				
				
				return $this->redirectToRoute('user_edit_a',array('id'=>$id));
			}
			
			if( in_array($role_array[$keys[0]], $role) && sizeof($role) != 1 ) // role student selected with other roles
			{
				$this->addFlash('warning',
								$translator->trans('Role Admin cannot have Employee role')
				);
				
				return $this->redirectToRoute('user_edit_a',array('id'=>$id));
			}
			
			$user->setRoles($role);	
			
            // Audit Logging //
            $audit = new Audit();
            $audit->setAction('EDIT USER');
            $audit->setTargetuser($user);
            $audit->setDatetime(date_create(date('Y-m-d H:i:s')));
            $audit->setUser($this->getCurrentUser());

            $em->persist($audit);
			$em->persist($user);
			$em->flush();
			
            $this->addFlash(
                'notice',
                $translator->trans('%US_UD%')
            );
			
			return $this->redirectToRoute('user_list');
		}
		
		return $this->render('EDUBundle:Form:edituser.html.twig',
							 array(	'user'	=> $user,
									'title' => $translator->trans('%EDIT_USER%'),
									'form' 	=> $form->createView()
        ));
	}
	
	
	
	
	/**
    *	emplyees list page
    *
    *@Route("/user/employees", name="user_employees")
    */
	public function listStudentAction(Request $request)
	{
		//translations
        $translator = $this->get('translator');
        $qb   = $this->getDoctrine()->getManager()->createQueryBuilder();

        $query  =   $qb
            ->select('u, s')
            ->from('EDUBundle:Users', 'u')
            ->leftJoin('u.department', 's')
            ->where('u.roles LIKE :rol')
            ->setParameter('rol','%"EMPLOYEE"%')
            ->getQuery();

        $result = $query->getResult();
		
	
		
        return $this->render('EDUBundle:users:layout_all_users.html.twig',
							array(
								'users' => $result,
								'title' => $translator->trans('All Employees'),
								'tables'=> 1
							)
        );
	}
	
	
	
	/**
    *Edit user page
    *
    *@Route("/useredit/{id}", name="user_edit")
    *@return	user edit form
    */
    public function edituserAction( $id, Request $request )
    {
        $user   = $this->getDoctrine()
						->getRepository('EDUBundle:Users')
						->find($id);
		
		// gets user information
		$user->setFirstname( $user->getFirstname() );
		$user->setLastname( $user->getLastname() );
		$user->setEmail( $user->getEmail() );
		$user->setRoles( $user->getRoles() );
		$user->setPhoneNumber( $user->getPhoneNumber() );
		
		
		$form = $this->createFormBuilder($user)
                ->add('firstname', TextType::class, array(
                                                            'attr'  =>  array('class' => 'form-control',
                                                                              'style' => 'margin:5px 0;')
                                                         ) )
                ->add('lastname', TextType::class, array(
                                                            'attr'  =>  array('class' => 'form-control',
                                                                              'style' => 'margin:5px 0;')
                                                         ) )
                ->add('email', EmailType::class, array(
                                                            'attr'  =>  array('class' => 'form-control',
                                                                              'style' => 'margin:5px 0;')
                                                         ) )
				->add('roles', ChoiceType::class, array(
                                                            'attr'  =>  array('class' => 'form-control',
                                                            'style' => 'margin:5px 0;'),
                                                            'choices' => 
                                                            array
                                                            (
                                                                'ROLE_ADMIN' => array
                                                                (
                                                                    $this->get('translator')->trans('Yes') => 'ROLE_ADMIN'
                                                                ),
                                                                'ROLE_TEACHER' => array
                                                                (
                                                                    $this->get('translator')->trans('Yes') => 'ROLE_TEACHER'
                                                                ),
                                                                'ROLE_STUDENT' => array
                                                                (
                                                                    $this->get('translator')->trans('Yes') => 'ROLE_STUDENT'
                                                                ),
                                                                'ROLE_PARENT' => array
                                                                (
                                                                    $this->get('translator')->trans('Yes') => 'ROLE_PARENT'
                                                                ),
                                                            ) 
                                                            ,
                                                            'multiple' => true,
                                                            'required' => true,
                                                            ) )
                /*->add('username', TextType::class, array(
                                                            'attr'  =>  array('class' => 'form-control',
                                                                              'style' => 'margin:5px 0;')
                                                         ) )
                ->add('password', TextType::class, array(
                                                            'attr'  =>  array('class' => 'form-control',
                                                                              'style' => 'margin:5px 0;')
                                                         ) )*/
                ->add('phonenumber', TextType::class, array(
                                                            'attr'  =>  array('class' => 'form-control',
                                                                              'style' => 'margin:5px 0;')
                                                         ) )
                ->add('Update User', SubmitType::class, array(
                                                            'attr'  =>  array('class' => 'btn btn-primary',
                                                                              'style' => 'margin:15px 0;')
                                                         ) )
                ->getForm();
        
        $form->handleRequest($request);
		//print_r($form);
		//die();
		
		if( $form->isSubmitted() && $form->isValid() )
		{
			$fname  = $form['firstname']->getData();
            $lname  = $form['lastname']->getData();
            $email  = $form['email']->getData();
			$role	= $form['roles']->getData();
            //$uname  = $form['username']->getData();
            //$pass   = $form['password']->getData();
            $phone  = $form['phonenumber']->getData();
			
			$em 	= $this->getDoctrine()->getManager();
			$user	= $em->getRepository('EDUBundle:Users')
                     ->find($id);
			
			// create new user
            $user->setFirstname($fname);
            $user->setLastname($lname);
            $user->setEmail($email);
			$user->setRoles($role);
            //$user->setUsername($uname);
            //$user->setPlainPassword($pass);
            $user->setPhoneNumber($phone);


            // Audit Logging //
            $audit = new Audit();
            $audit->setAction('EDIT USER');
            $audit->setTargetuser($user);
            $audit->setDatetime(date_create(date('Y-m-d H:i:s')));
            $audit->setUser($this->getCurrentUser());

            $em->persist($audit);
			
			$em->flush();
            
            $this->addFlash(
                'notice',
                $translator->trans('%US_UD%')
            );
			
			return $this->redirectToRoute('user_list');
		}
		
        return $this->render('EDUBundle:users:layout_edit_user.html.twig',
							array(
								array(	'user'	=>	$user,
										'form'  =>  $form->createView()
								),
								'title' => 'Edit User',
								'form'  =>  $form->createView()
							)
        );
    }
	
	
	/**
     *Delete User Page
     *
     *@Route("/user/delete/{id}", name="user_delete")
     */
	public function deleteuserAction( $id )
	{
		//translations
        $translator = $this->get('translator');
		
		$em     = $this->getDoctrine()->getManager();
        $user   = $em->getRepository('EDUBundle:Users')->find($id);
        
        return $this->render(
                            'EDUBundle:users:layout_user_delete.html.twig',
                            array(
                                'user' => $user,
                                'title' => $translator->trans('%User%'),
                                'tables' => 1
                            )
        );
	}
	
	
	/**
     *Delete User Page
     *
     *@Route("/delete/{id}", name="delete_user")
     */
	public function deleteAction( $id )
	{
		//translations
        $translator = $this->get('translator');
		
		$em     = $this->getDoctrine()->getManager();
        $user   = $em->getRepository('EDUBundle:Users')->find($id);
		$role	= $user->getRoles();
		
		// deleting ROLE_STUDENT
		if( in_array('ROLE_STUDENT', $role) ) // the user is a STUDENT
		{
			// delete from the STUDENT table
			$student = $em->getRepository('EDUBundle:Student')->findOneBy( array('users' => $id) );
			if( $student ) // student is existing in the STUDENT table
			{
				$em->remove($student);
			}
		}
		
		// deleting ROLE_TEACHER
		if( in_array('ROLE_TEACHER', $role) ) // the user is a TEACHER
		{
			$classNames = array(); // teacher assigned class names
			//$subNames	= array(); // teacher assigned subject names
			$schedNames	= array(); // teacher assigned schedules
			$stdNames	= array(); // teacher assigned students as parent
			$errorMsg	= array(); // list of error messages as an array
			
			// check if the teacher is assgined to classes
			$classes = $em->getRepository('EDUBundle:Teacher')->findBy( array('users' => $id) );
			if( $classes ) // yes the teacher assigned to classes
			{
				foreach( $classes as $class )
				{
					$classNames[] = $class->getClasses()->getClassName();
				}
				
				if( $classNames )
				{
					$errorMsg[] = $translator->trans( 	'Teacher can not be deleted. Already assigned to %classes% class(es)',
														array( '%classes%'	=> implode(', ', $classNames) ) );
				}
				
			}
			
			// check if the teacher is assgined to subjects
			$subs = $em->getRepository('EDUBundle:TeacherSubjects')->findBy( array('teachers' => $id) );
			if( $subs ) // yes the teacher assigned to subjects
			{
				foreach( $subs as $sub )
				{
					//$subNames[] = $sub->getSubjects()->getName();
					$em->remove($sub);
					$em->flush();
				}
				
				//if( $subNames )
				//{
				//	$errorMsg[] = $translator->trans( 	'Teacher can not be deleted. Already assigned to %subjects% subject(s)',
				//										array( '%subjects%'	=> implode(', ', $subNames) ) );
				//}
			}
			
			
			// check if the teacher is assgined as parent
			$parents = $em->getRepository('EDUBundle:Student')->findBy( array('parents' => $id) );
			if( $parents ) // yes the teacher assigned to subjects
			{
				foreach( $parents as $parent )
				{
					$stdNames[] = $parent->getUsers()->getFirstname() . ' ' .  $parent->getUsers()->getLastname();
				}
				
				if( $stdNames )
				{
					$errorMsg[] = $translator->trans( 	'Teacher can not be deleted. Already assigned as parent for %students% student(s)',
														array( '%students%'	=> implode(', ', $stdNames) ) );
				}
			}
			
			if( $errorMsg ) // Teacher cannot delete as assigned to Classes, Subjects or schedules
			{
				foreach( $errorMsg as $message )
				{
					$this->addFlash('warning', $message );
				}
				
				return $this->redirectToRoute('user_delete',
											  array( 'id' => $id ));
			}
		}
		
		// deleting ROLE_PARENT
		if( in_array('ROLE_PARENT', $role) ) // the user is a PARENT
		{
			$childrens = array();
			
			$childs = $em->getRepository('EDUBundle:Student')->findBy( array('parents' => $id) );
			
			if( $childs ) // the parent has assigned students
			{
				foreach( $childs as $child )
				{
					$childrens[] = $child->getUsers()->getFirstname() . ' ' .  $child->getUsers()->getLastname();
				}
				
				$this->addFlash('warning',
								$translator->trans('Parent cannot be deleted. Already assigned as parent for %childs% student(s)',
												   array( '%childs%' => implode(', ', $childrens) )));
				
				return $this->redirectToRoute('user_delete',
											  array( 'id' => $id ));
			}
		}
		
        // Audit Logging //
        $audit = new Audit();
        $audit->setAction('DELETE USER');
        $audit->setTargetuser($user);
        $audit->setDatetime(date_create(date('Y-m-d H:i:s')));
        $audit->setUser($this->getCurrentUser());
        $em->persist($audit);
        
		$user->setEnabled('0');
		$em->flush();
        
        return $this->redirectToRoute('user_list');
    }
	
	//----------------------- PROTECTED FUNCTIONS ------------------------------
	
	/**
	* Get user id
	* @return integer $userId
	*/
	public function getUserId()
	{
		$user 	= $this->get('security.token_storage')->getToken()->getUser();
        $userId = $user->getId();
		
		return $userId;
	}
	
	/**
	* Get current user
	* @return user
	*/
	public function getCurrentUser()
	{
		if($this->get('security.token_storage') && $this->get('security.token_storage')->getToken()){
			$user 	= $this->get('security.token_storage')->getToken()->getUser();
			return $user;
		}
		else
			return false;
	}
	
	/**
	 * Get all admins
	 * @param 	null
	 * @return	all admins as objects
	 */
	public function getAdmins()
	{
		$query = $this->getDoctrine()->getEntityManager()
						->createQuery('SELECT u FROM EDUBundle:Users u WHERE u.roles LIKE :role')
						->setParameter('role', '%"ROLE_ADMIN"%' );
		return $users = $query->getResult();
	}
	
	/**
	 *@abstract	Check the given class and subject assigned in class_subject table
	 *@param	class, subject
	 *@return	TRUE(is exists) else FALSE
	 */
	function classHasSub( $class, $subject )
	{
		$em 	= $this->getDoctrine()->getManager();
        $result	= $em->getRepository('EDUBundle:ClassSubject')
					->findBy( array(	'class' 	=> $class,
										'subjects'	=> $subject ) );
		if( $result )
			return TRUE;
		else
			return FALSE;
	}
	
	
	/**
	 *@abstract	Check the given teacher and class assigned in teacher table
	 *@param	teacher, subject
	 *@return	TRUE(is exists) else FALSE
	 */
	function teacherHasClass( $teacher, $class )
	{
		$em 	= $this->getDoctrine()->getManager();
        $result	= $em->getRepository('EDUBundle:Teacher')
					->findBy( array('users'		=> $teacher,
									'classes'	=> $class ) );
		if( $result )
			return TRUE;
		else
			return FALSE;
	} 
	
}
