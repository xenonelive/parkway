<?php

namespace EDUBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Audit
 *
 * @ORM\Table(name="audit")
 * @ORM\Entity(repositoryClass="EDUBundle\Repository\AuditRepository")
 */
class Audit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=255)
     */
    private $action;

    /**
     * @ORM\ManyToOne(targetEntity="Users")
     */
    private $targetuser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;

    /**
     * @ORM\ManyToOne(targetEntity="Users")
     */
    private $user;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return Audit
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Audit
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set targetuser
     *
     * @param \EDUBundle\Entity\Users $targetuser
     *
     * @return Audit
     */
    public function setTargetuser(\EDUBundle\Entity\Users $targetuser = null)
    {
        $this->targetuser = $targetuser;

        return $this;
    }

    /**
     * Get targetuser
     *
     * @return \EDUBundle\Entity\Users
     */
    public function getTargetuser()
    {
        return $this->targetuser;
    }

    /**
     * Set user
     *
     * @param \EDUBundle\Entity\Users $user
     *
     * @return Audit
     */
    public function setUser(\EDUBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \EDUBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }
}
