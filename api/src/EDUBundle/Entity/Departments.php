<?php

namespace EDUBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departments
 *
 * @ORM\Table(name="departments")
 * @ORM\Entity(repositoryClass="EDUBundle\Repository\DepartmentsRepository")
 */
class Departments
{

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Departments")
     * @ORM\JoinColumn(name="parentid", referencedColumnName="id" )
     */
    private $parentid;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Departments
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parentid
     *
     * @param \EDUBundle\Entity\Departments $parentid
     *
     * @return Departments
     */
    public function setParentid(\EDUBundle\Entity\Departments $parentid = null)
    {
        $this->parentid = $parentid;

        return $this;
    }

    /**
     * Get parentid
     *
     * @return \EDUBundle\Entity\Departments
     */
    public function getParentid()
    {
        return $this->parentid;
    }
}
