#!/bin/bash
php /var/www/parkway/api/bin/console doctrine:generate:entities EDUBundle
php /var/www/parkway/api/bin/console doctrine:schema:update --force
#php /var/www/parkway/api/bin/console translation:extract fr --config=EDUBundle --output-format=xliff &&
#php /var/www/parkway/api/bin/console translation:extract en --config=EDUBundle --output-format=xliff &&
#php /var/www/parkway/api/bin/console translation:extract fr --config=EDUBundle --output-format=xlf
#php /var/www/parkway/api/bin/console translation:extract en --config=EDUBundle --output-format=xlf
chmod +x /var/www/parkway/api/bin/console &&
/var/www/parkway/api/bin/console --env=prod cache:clear &&
/var/www/parkway/api/bin/console cache:clear &&
find /var/www/parkway/api/ -type f -exec chmod 0664 {} + &&
find /var/www/parkway/api/ -type d -exec chmod 2775 {} + &&
chmod +x /var/www/parkway/api/bin/console;
